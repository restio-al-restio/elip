NEWS for ELIP, the Emacs Learning Instruction Program

Copyright (C) 2008-2021 Thien-Thi Nguyen
See the end for copying conditions.


- 0.806 | 2020-11-06

  - distribution now .tar.lz

    If you have GNU tar, you can use "tar xf" and it will DTRT.
    If not, you can use "lzip -dc TARBALL | tar xf -" to unpack
    the .tar.lz.

  - support "make uninstall"

    Now you have no excuse to not try ELIP.  :-D

  - bootstrap/maintenance tools

    upgraded:

     GNU gnulib 2020-11-04 23:13:40
     GNU Autoconf 2.69d

    as before:

     GNU texinfo 6.7
     GNU Emacs 26.1


- 0.805 | 2020-10-09

  - new example: JLPT level 4

    See files examples/jlpt-4.{README,elip,sjs}.

    This may help those facing the Japanese Language Proficiency Test
    (level 4) to study.  The contributor is only known by "Chris", so
    if you are *THAT* Chris and would like to have your surname added
    to the THANKS file, please let me know what it is!

  - ‘cl’ no longer required

    ELIP uses ‘cl-lib’ now.

  - bootstrap/maintenance tools

    upgraded:

     GNU gnulib 2020-10-03 19:51:08
     GNU Autoconf 2.69c
     GNU texinfo 6.7
     GNU Emacs 26.1

    as before:

     (none)


- 0.804 | 2009-11-04
  - configure script checks for EDB (required; error if not found)
  - new var: elip-question-face
  - "learn new" and "learn old" also display total item count
  - (internal, performance) date rep transformations faster


- 0.803 | 2008-05-26
  - dependency upgrade: EDB 1.31 or later (see README)
  - elip-import trims leading/trailing whitespace
  - learning-session enhancements
    - non-"text mode"
      - ELIP displays what you type in as "Your Answer"
      - database-local variable `input-method' respected
    - "text mode"
      - omitted text (and underscores) highlighted
      - before/after displays in-place


- 0.802 | 2008-03-21
  - examples/*.elip no longer use home-grown ASCIIfication
    - spanish.elip uses Spanish characters
    - esperanto.elip and egrammar.elip use Esperanto characters


- 0.801 | 2008-01-22
  - maintainership transfer from Bob Newell to ttn
  - license GPLv3+ (see COPYING)
  - updated requirements: Emacs 22, EDB 1.29
  - partial redesign (NOTE: INCOMPATIBLE w/ ELIP 0.412beta)
    - dropped command: elip-debug-data
    - new command: elip-session
    - database file self-contained
      - extension conventionally ".elip"
      - no more .dat, .dba, .fmt files
    - unused record fields pruned
    - parameters database-local (no longer global)
    - better quit (C-g) handling
    - clickable buttons in help, report buffers
    - (featurep 'elip) => t
  - documentation overhaul
    - reorganized, slightly rewritten
    - source format: texinfo
    - distribution formats: info (installed), pdf
    - new features: xrefs, urls, index (concept/function)
  - examples regularized: whitespace, punctuation
  - standard installation (see README)
  - maintenance uses git: http://www.gnuvola.org/wip/


- 0.412beta
  - see http://www.bobnewell.net/elisp.html
  - see ChangeLog.OLD


- etc

Copyright information:

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


	Local Variables:
	mode: outline
	outline-regexp: "\\([ ][ ]\\)*- "
	fill-column: 72
	fill-prefix: "\t"
	End:
