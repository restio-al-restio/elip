-*-coding:japanese-shift-jis-*-

Q. ああ
A. (int) Ah!/Oh!/Alas!/
Q. 会う
A. [あう] /(v5u) to meet/to interview/(P)/
Q. 青い
A. [あおい] /(adj) blue/pale/green/unripe/inexperienced/(P)/
Q. 赤い
A. [あかい] /(adj) red/(P)/
Q. 明い
A. [あかるい]
Q. 秋
A. [あき] /(n-adv) autumn/fall/(P)/
Q. 開く
A. [あく] /(v5k) to open (e.g. a festival)/(P)/
Q. 開ける
A. [あける] /(v1) to become civilized/to become opened up/be up-to-date/(P)/
Q. 上げる
A. [あげる] /(v1) to give/to raise/to elevate/to fly (kites)/to praise/to increase/to advance/to promote/to vomit/to usher in/to admit/to send (to school)/to offer/to present/to leave with/to finish/to arrange (expenses)/to observe/to perform/to quote/to mention/to bear (a child)/to improve (talents)/to do up (the hair)/to arrest/to engage/to fry/(rains) to stop/(P)/
Q. 朝
A. [あさ] /(n-adv,n-t) morning/(P)/
Q. 朝御飯
A. [あさごはん] /(n) breakfast/(P)/
Q. あさって
A. (n-adv,n-t) day after tomorrow/(P)/
Q. 足
A. [あし] /(n) foot/pace/gait/leg/(P)/
Q. あした
A. (n-t) tomorrow/(P)/
Q. あそこ
A. (n) (1) (uk) there/over there/that place/yonder/(2) (X) (col) genitals/(P)/
Q. 遊ぶ
A. [あそぶ] /(v5b) (1) to play/to make a visit (esp. for pleasure)/(2) to be idle/to do nothing/(P)/
Q. 暖かい
A. [あたたかい] /(adj) warm/mild/genial/(P)/
Q. 頭
A. [あたま] /counter for large animals/(P)/
Q. 新しい
A. [あたらしい] /(adj) new/(P)/
Q. あちら
A. (n) (1) there/yonder/that/(P)/
Q. 暑い
A. [あつい] /(adj) hot/warm/(P)/
Q. 熱い
A. [あつい] /(adj) hot (thing)/(P)/
Q. 厚い
A. [あつい] /(adj) cordial/kind/warm(hearted)/thick/deep/(P)/
Q. 後
A. [あと] /(adj-no,n) afterwards/since then/in the future/(P)/
Q. あなた
A. (n) (1) (uk) the other/the other side/(2) there/yonder/that/
Q. 兄
A. [あに] /(n) (hum) older brother/(P)/
Q. 姉
A. [あね] /(n) (hum) older sister/(P)/
Q. あの
A. [（指示詞）] /(adj-pn) (uk) that over there/(P)/
Q. あの
A. [（感動詞）] /(adj-pn) (uk) that over there/(P)/
Q. アパート
A. (n,adv) (1) apartment (abbr)/(2) apartment building (abbr)/(3) apart/(P)/
Q. あびる
A. (v1) to bathe/to bask in the sun/to shower/(P)/
Q. 危ない
A. [あぶない] /(adj) dangerous/critical/grave/uncertain/unreliable/limping/narrow/close/watch out!/(P)/
Q. 甘い
A. [あまい] /(adj) delicious/
Q. あまり
A. [（副詞）] /(adj-na,adv,n,n-suf) (uk) not very (this form only used as adverb)/not much/remainder/rest/remnant/surplus/balance/excess/remains/scraps/residue/fullness/other/too much/(P)/
Q. 雨
A. [あめ] /(n) rain/(P)/
Q. 洗う
A. [あらう] /(v5u) to wash/(P)/
Q. ある
A. (v5r-i) (uk) to be/to have/(P)/
Q. 歩く
A. [あるく] /(v5k) to walk/(P)/
Q. あれ
A. [（指示詞）] /(n) stormy weather/tempest/chaps (of skin)/
Q. いい
A. (adj) (col) (uk) good/(P)/
Q. よい
A. (adj) good/nice/pleasant/ok/(P)/
Q. いいえ
A. (int,n) (uk) no/nay/yes/well/(P)/
Q. 言う
A. [いう] /(v5u) to say/(P)/
Q. 家
A. [いえ] /(suf) house/family/(P)/
Q. 行く
A. [いく] /(v5k-s) to go/(P)/
Q. 行く
A. [ゆく] /(v5k-s) to go/(P)/
Q. いくつ
A. (n) how many?/how old?/(P)/
Q. いくら
A. (adv,n) how much?/how many?/(P)/
Q. 池
A. [いけ] /(n) pond/(P)/
Q. 医者
A. [いしゃ] /(n) doctor (medical)/(P)/
Q. いす
A. (n) chair/(P)/
Q. 忙しい
A. [いそがしい] /(adj) busy/irritated/
Q. 痛い
A. [いたい] /(adj) painful/(P)/
Q. 一
A. [いち] /(num) one/(P)/
Q. 一日
A. [いちにち] /(n) (1) first of month/(P)/
Q. いちばん
A. (n-adv) (1) best/first/number one/(2) a game/a round/a bout/a fall/an event (in a meet)/(P)/
Q. いつ
A. be lost/peace/hide/mistake/beautiful/in turn/
Q. 五日
A. [いつか] /(n) five days/the fifth day (of the month)/(P)/
Q. 一緒
A. [いっしょ] /(adv,n) together/meeting/company/(P)/
Q. 五つ
A. [いつつ] /(n) five/(P)/
Q. いつも
A. (adv,n) always/usually/every time/never (with neg. verb)/(P)/
Q. 今
A. [いま] /this/now/
Q. 意味
A. [いみ] /(n,vs) meaning/significance/(P)/
Q. 妹
A. [いもうと] /(n) (hum) younger sister/(P)/
Q. 嫌
A. [いや] /(adj-na,n) disagreeable/detestable/unpleasant/reluctant/(P)/
Q. 入口
A. [いりぐち] /(n) entrance/gate/approach/mouth/
Q. 居る
A. [いる] /(v5r) (uk) (hum) to be (animate)/to be/to exist/(P)/
Q. 要る
A. [いる] /(v5r) to need/(P)/
Q. 入れる
A. [いれる] /(v1) to put in/to take in/to bring in/to let in/to admit/to introduce/to commit (to prison)/to usher in/to insert/to set (jewels)/to employ/to listen to/to tolerate/to comprehend/to include/to pay (interest)/to cast (votes)/(P)/
Q. 色
A. [いろ] /(n) (1) colour/(2) sensuality/lust/(P)/
Q. いろいろ
A. (adj-na,adj-no,adv,n) various/
Q. 上
A. [うえ] /(n,pref,suf) (1) first volume/(2) superior quality/(3) governmental/imperial/top/best/high class/going up/presenting/showing/aboard a ship or vehicle/from the standpoint of/as a matter of (fact)/(P)/
Q. 後
A. [うしろ] /(adj-no,n) afterwards/since then/in the future/(P)/
Q. 薄い
A. [うすい] /(adj) thin/weak/watery/diluted/(P)/
Q. 歌
A. [うた] /(n) song/poetry/(P)/
Q. 歌う
A. [うたう] /(v5u) to sing/(P)/
Q. うち
A. [（わたしのうち）] /(n) inside/
Q. 生まれる
A. [うまれる] /(v1) to be born/(P)/
Q. 海
A. [うみ] /(n) sea/beach/(P)/
Q. 売る
A. [うる] /(v5r) to sell/(P)/
Q. 上着
A. [うわぎ] /(n) coat/tunic/jacket/outer garment/(P)/
Q. 絵
A. [え] /(n,n-suf) picture/drawing/painting/sketch/
Q. 映画
A. [えいが] /(n) movie/film/(P)/
Q. 映画館
A. [えいがかん] /(n) movie theatre (theater)/cinema/(P)/
Q. 英語
A. [えいご] /(n) the English language/(P)/
Q. ええ
A. (conj,int,n) yes/(P)/
Q. 駅
A. [えき] /(n) station/(P)/
Q. エレベーター /
A. (n) elevator/(P)/
Q. ~円
A. [~えん]
Q. 鉛筆
A. [えんぴつ] /(n) pencil/(P)/
Q. お~
A. [（接頭語）]
Q. おいしい
A. (adj) delicious/tasty/(P)/
Q. 大きい
A. [おおきい] /(adj) big/large/great/(P)/
Q. 大勢
A. [おおぜい] /(n) great number of people/
Q. お母さん
A. [おかあさん] /(n) (hon) mother/(P)/
Q. お菓子
A. [おかし] /(n) confections/sweets/candy/(P)/
Q. お金
A. [おかね] /(n) money/(P)/
Q. 起きる
A. [おきる] /(v1) to get up/to rise/(P)/
Q. 置く
A. [おく] /(v5k) to put/to place/(P)/
Q. 奥さん
A. [おくさん] /(n) (hon) wife/your wife/his wife/married lady/madam/(P)/
Q. お酒
A. [おさけ]
Q. お皿
A. [おさら]
Q. 伯父
A. [おじ] /(n) (hum) uncle (older than one's parent)/
Q. 叔父
A. [おじ] /(n) uncle (younger than one's parent)/(P)/
Q. おじいさん
A. (n) grandfather/male senior-citizen/
Q. 教える
A. [おしえる] /(v1) to teach/to inform/to instruct/(P)/
Q. 押す
A. [おす] /(v5s,vt) to push/to press/to stamp (i.e. a passport)/(P)/
Q. 遅い
A. [おそい] /(adj) late/slow/(P)/
Q. お茶
A. [おちゃ] /(n) tea (green)/(P)/
Q. お手洗い
A. [おてあらい] /toilet/restroom/lavatory/bathroom (US)/(P)/
Q. お父さん
A. [おとうさん] /(n) (hon) father/(P)/
Q. 弟
A. [おとうと] /(n) younger brother/faithful service to those older/brotherly affection/
Q. 男
A. [おとこ] /(n) man/(P)/
Q. 男の子
A. [おとこのこ] /(n) boy/(P)/
Q. おととい
A. (n-adv,n-t) day before yesterday/(P)/
Q. おととし
A. (n-adv,n-t) year before last/(P)/
Q. 大人
A. [おとな] /(n) adult/
Q. おなか
A. (n) stomach/
Q. 同じ
A. [おなじ] /(adj-na,n) same/identical/equal/uniform/equivalent/similar/common (origin)/changeless/(P)/
Q. お兄さん
A. [おにいさん] /(n) (hon) older brother/(vocative) "Mister?"/(P)/
Q. お姉さん
A. [おねえさん] /(n) (hon) older sister/(vocative) "Miss?"/(P)/
Q. 伯母
A. [おば] /(n) (hum) aunt (older than one's parent)/
Q. 叔母
A. [おば] /(n) aunt (younger than one's parent)/(P)/
Q. おばあさん
A. (n) grandmother/female senior-citizen/
Q. お弁当
A. [おべんとう]
Q. 覚える
A. [おぼえる] /(v1) to remember/to memorize/(P)/
Q. 重い
A. [おもい] /(adj) (1) heavy/massive/(2) serious/important/severe/oppressed/(P)/
Q. おもしろい
A. (adj) interesting/amusing/(P)/
Q. 泳ぐ
A. [およぐ] /(v5g) to swim/(P)/
Q. 降りる
A. [おりる] /(v1) to alight (e.g. from bus)/to get off/to descend (e.g. a mountain)/(P)/
Q. 終る
A. [おわる] /(io) (v5r) to finish/to close/
Q. 音楽
A. [おんがく] /(n) music/musical movement/(P)/
Q. 女
A. [おんな] /(n) woman/girl/daughter/
Q. 女の子
A. [おんなのこ] /(n) girl/(P)/
Q. ~回
A. [~かい]
Q. ~階
A. [~かい]
Q. 外国
A. [がいこく] /(n) foreign country/(P)/
Q. 外国人
A. [がいこくじん] /(n) foreigner/(P)/
Q. 会社
A. [かいしゃ] /(n) company/corporation/(P)/
Q. 階段
A. [かいだん] /(n) stairs/(P)/
Q. 買い物
A. [かいもの] /(n) shopping/(P)/
Q. 買う
A. [かう] /(v5u) to buy/(P)/
Q. 返す
A. [かえす] /(v5s,vt) to return something/(P)/
Q. 帰る
A. [かえる] /(v5r) to go back/to go home/to come home/to return/(P)/
Q. 顔
A. [かお] /(n) face (person)/(P)/
Q. かかる
A. [（時間がかかる）] /(v5r) to suffer from/(P)/
Q. かぎ
A. (n) key/(P)/
Q. 書く
A. [かく] /(v5k) to write/(P)/
Q. 学生
A. [がくせい] /(n) student/(P)/
Q. ~か月
A. [~かげつ]
Q. かける
A. [（眼鏡をかける）] /(v5r) to soar/to fly/
Q. かける
A. [（電話をかける）] /(v5r) to soar/to fly/
Q. 傘
A. [かさ] /(n) umbrella/parasol/(P)/
Q. 貸す
A. [かす] /(v5s) to lend/(P)/
Q. 風
A. [かぜ] /(adj-na,n,n-suf) method/manner/way/(P)/
Q. 家族
A. [かぞく] /(n) family/members of a family/(P)/
Q. かた
A. [（この方）] /(adj-na,n) many/plentiful/
Q. 片仮名
A. [かたかな] /(n) katakana/(P)/
Q. ~月
A. [~がつ]
Q. 学校
A. [がっこう] /(n) school/(P)/
Q. 角
A. [かど] /(n) horn/(P)/
Q. 家内
A. [かない] /(n) (hum) wife/(P)/
Q. かばん
A. (n) bag/satchel/briefcase/basket/(P)/
Q. 花瓶
A. [かびん] /(n) (flower) vase/(P)/
Q. かぶる
A. (v5r) to wear/to put on (head)/to pour or dash water (on oneself)/(P)/
Q. 紙
A. [かみ] /(n) paper/(P)/
Q. カメラ
A. (n) camera/(P)/
Q. 火曜日
A. [かようび] /(n-adv,n) Tuesday/(P)/
Q. 辛い
A. [からい] /(adj) painful/heart-breaking/(P)/
Q. 体
A. [からだ] /(n) appearance/air/condition/state/form/
Q. 借りる
A. [かりる] /(v1) to borrow/to have a loan/to hire/to rent/to buy on credit/(P)/
Q. ~がる
Q. 軽い
A. [かるい] /(adj) light/non-serious/minor/(P)/
Q. カレンダー
A. (n) calendar/(P)/
Q. 川
A. [かわ] /(n) river/(P)/
Q. 河
A. [かわ] /(n) river/stream/(P)/
Q. ~側
A. [~がわ]
Q. かわいい
A. (adj) (sl) pretty/cute/lovely/charming/dear/darling/pet/(P)/
Q. 漢字
A. [かんじ] /(n) Chinese characters/kanji/(P)/
Q. 木
A. [き] /(n) tree/wood/timber/(P)/
Q. 黄色い
A. [きいろい] /(adj) yellow/(P)/
Q. 消える
A. [きえる] /(v1) to go out/to vanish/to disappear/(P)/
Q. 聞く
A. [きく] /(v5k) to hear/to listen/to ask/(P)/
Q. 北
A. [きた] /north/
Q. ギター
A. (n) guitar/(P)/
Q. 汚い
A. [きたない] /(adj) dirty/unclean/filthy/(P)/
Q. 喫茶店
A. [きっさてん] /(n) coffee lounge/(P)/
Q. 切手
A. [きって] /man of ability/
Q. 切符
A. [きっぷ] /(n) ticket/(P)/
Q. 昨日
A. [きのう] /(n-adv,n-t) yesterday/(P)/
Q. 九
A. [きゅう] /(num) nine/(P)/
Q. 牛肉
A. [ぎゅうにく] /(n) beef/(P)/
Q. 今日
A. [きょう] /(n-t) today/this day/(P)/
Q. 教室
A. [きょうしつ] /(n) classroom/(P)/
Q. 兄弟
A. [きょうだい] /(n) (hum) siblings/(P)/
Q. 去年
A. [きょねん] /(n-adv,n-t) last year/
Q. 嫌い
A. [きらい] /(adj-na,n) dislike/hate/(P)/
Q. 切る
A. [きる] /(suf,v5r) to cut/to chop/to hash/to carve/to saw/to clip/to shear/to slice/to strip/to fell/to cut down/to punch/to sever (connections)/to pause/to break off/to disconnect/to turn off/to hang up/to cross (a street)/to discount/to sell below cost/to shake (water) off/to finish/to be through/to complete/(P)/
Q. 着る
A. [きる] /(v1) to wear/to put on (from shoulders down)/(P)/
Q. きれい
A. (oK) (adj-na) (uk) pretty/clean/nice/tidy/beautiful/fair/
Q. キロ
A. [（キログラム）] /(n,pref) (abbr) kilo-/kilogram/kilometre/10^3/(P)/
Q. キロ
A. [（キロメートル）] /(n,pref) (abbr) kilo-/kilogram/kilometre/10^3/(P)/
Q. 銀行
A. [ぎんこう] /(n) bank/(P)/
Q. 金曜日
A. [きんようび] /(n-adv,n) Friday/(P)/
Q. 工
A. [く]
Q. 薬
A. [くすり] /(n) medicine/(P)/
Q. ください
A. please (kana only)/(with te-form verb) please do for me/
Q. 果物
A. [くだもの] /(n) fruit/(P)/
Q. 口
A. [くち] /(n) mouth/orifice/opening/(P)/
Q. 靴
A. [くつ] /(n) shoes/footwear/(P)/
Q. 靴下
A. [くつした] /(n) socks/(P)/
Q. 国
A. [くに] /(n) country/(P)/
Q. 曇る
A. [くもる] /(v5r) to become cloudy/to become dim/(P)/
Q. 暗い
A. [くらい] /(adj) (uk) dark/gloomy/(P)/
Q. ~くらい
Q. ~ぐらい
Q. クラス
A. (n) class/(P)/
Q. グラム
A. (n) gram/gramme/(P)/
Q. 来る
A. [くる] /(vk) to come/to come to hand/to arrive/to approach/to call on/to come on (rain)/to set in/to be due/to become/to grow/to get/to come from/to be caused by/to derive from/(P)/
Q. 車
A. [くるま] /(n) car/vehicle/wheel/(P)/
Q. 黒い
A. [くろい] /(adj) (1) black/(2) dark/(P)/
Q. 今朝
A. [けさ] /(ik) (n-t) this morning/(P)/
Q. 消す
A. [けす] /(v5s) to erase/to delete/to turn off power/(P)/
Q. 結構
A. [けっこう] /(adj-na,n-adv,n) (1) (uk) splendid/nice/wonderful/delicious/sweet/(2) (arch) construction/architecture/(3) well enough/tolerably/(P)/
Q. 結婚
A. [けっこん] /(adj-no,n,vs) marriage/(P)/
Q. 月曜日
A. [げつようび] /(n-adv,n) Monday/(P)/
Q. 玄関
A. [げんかん] /(n) entranceway/entry hall/(P)/
Q. 元気
A. [げんき] /(adj-na,n) health(y)/robust/vigor/energy/vitality/vim/stamina/spirit/courage/pep/(P)/
Q. ~個
A. [~こ]
Q. 五
A. [ご] /(num) five/(P)/
Q. ~語
A. [~ご]
Q. 公園
A. [こうえん] /(n) (public) park/(P)/
Q. 交番
A. [こうばん] /(n) police box/(P)/
Q. 声
A. [こえ] /(n) voice/(P)/
Q. コート
A. (n) coat/tennis court/(P)/
Q. ここ
A. [（指示詞）] /(n) cry of a baby at its birth/
Q. 午後
A. [ごご] /(n-adv,n-t) afternoon/p.m./pm/(P)/
Q. 九日
A. [ここのか] /nine days/the ninth day (of the month)/(P)/
Q. 九つ
A. [ここのつ] /(n) nine/(P)/
Q. 御主人
A. [ごしゅじん] /(n) (hon) your husband/her husband/
Q. 午前
A. [ごぜん] /(n-adv,n-t) morning/A.M./am/(P)/
Q. 答える
A. [こたえる] /(v1) to answer/to reply/
Q. こちら
A. (n) (1) (uk) this person/(2) this direction/(3) this side/(4) thereafter/(P)/
Q. コップ
A. (n) (a) glass (nl: Kop)/(P)/
Q. 今年
A. [ことし] /(n-adv,n-t) this year/
Q. 言葉
A. [ことば] /(n) word(s)/language/speech/(P)/
Q. 子供
A. [こども] /(n) child/children/(P)/
Q. この
A. (adj-pn,int) (uk) this/(P)/
Q. 御飯
A. [ごはん] /(n) rice (cooked)/meal/(P)/
Q. 困る
A. [こまる] /(v5r) to be worried/to be bothered/(P)/
Q. これ
A. (int,n) (uk) this/
Q. 今月
A. [こんげつ] /(n-adv,n-t) this month/(P)/
Q. 今週
A. [こんしゅう] /(n-adv,n-t) this week/(P)/
Q. こんな
A. (adj-na,adj-pn,adv,n) such/like this/(P)/
Q. 今晩
A. [こんばん] /(n-adv,n-t) tonight/this evening/(P)/
Q. さあ
A. [（感動詞）] /(conj,int) come (int)/come now/(P)/
Q. ~歳
A. [~さい]
Q. 魚
A. [さかな] /(n) fish/(P)/
Q. 先
A. [さき] /(adj-no,n) the future/priority/precedence/former/previous/old/late/
Q. 咲く
A. [さく] /(v5k) to bloom/(P)/
Q. 作文
A. [さくぶん] /(n) composition/writing/(P)/
Q. さす
A. [（傘をさす）] /(v5s) to light (a fire)/to apply moxa cautery/
Q. ~冊
A. [~さつ]
Q. 雑誌
A. [ざっし] /(n) journal/magazine/(P)/
Q. 砂糖
A. [さとう] /(n) sugar/(P)/
Q. 寒い
A. [さむい] /(adj) cold (e.g. weather)/(P)/
Q. 再来年
A. [さらいねん] /(n-adv,n-t) year after next/(P)/
Q. ~さん
Q. 三
A. [さん] /(num) three/(P)/
Q. 散歩
A. [さんぽ] /(n,vs) walk/stroll/(P)/
Q. 四
A. [し] /(num) four/(P)/
Q. ~時
A. [~じ]
Q. 塩
A. [しお] /(n) salt/(P)/
Q. しかし
A. (conj) (uk) however/but/(P)/
Q. 時間
A. [じかん] /(n-adv,n) time/(P)/
Q. ~時間
A. [~じかん]
Q. 仕事
A. [しごと] /(adj-no,n) work/occupation/employment/(P)/
Q. 辞書
A. [じしょ] /(n) dictionary/lexicon/(P)/
Q. 静か
A. [しずか] /(adj-na) quiet/peaceful/(P)/
Q. 下
A. [した] /(n) under/below/beneath/(P)/
Q. 七
A. [しち] /(num) seven/(P)/
Q. 質問
A. [しつもん] /(n,vs) question/inquiry/(P)/
Q. 自転車
A. [じてんしゃ] /(n) bicycle/(P)/
Q. 自動車
A. [じどうしゃ] /(n) automobile/(P)/
Q. 死ぬ
A. [しぬ] /(v5n) to die/(P)/
Q. 字引
A. [じびき] /(n) dictionary/(P)/
Q. 自分
A. [じぶん] /(n) myself/oneself/(P)/
Q. 閉まる
A. [しまる] /(v5r,vi) to close/to be closed/(P)/
Q. 閉める
A. [しめる] /(v1,vt) to close/to shut/(P)/
Q. 締める
A. [しめる] /(v1) to tie/to fasten/(P)/
Q. じゃ
A. [（感動詞）]
Q. じゃあ
A. [（感動詞）] /(conj,int) well/well then/(P)/
Q. 写真
A. [しゃしん] /(n) photograph/(P)/
Q. シャツ
A. (n) shirt/singlet/(P)/
Q. 十
A. [じゅう] /(num) 10/ten/(P)/
Q. ~週間
A. [~しゅうかん]
Q. 授業
A. [じゅぎょう] /(n,vs) lesson/class work/(P)/
Q. 宿題
A. [しゅくだい] /(n) homework/(P)/
Q. 上手
A. [じょうず] /(adj-na,n) skill/skillful/dexterity/(P)/
Q. 丈夫
A. [じょうぶ] /(adj-na,n) (1) hero/gentleman/warrior/manly person/(2) good health/robustness/strong/solid/durable/
Q. しょうゆ
A. (iK) (n) soy sauce/
Q. 食堂
A. [しょくどう] /(n) cafeteria/dining hall/(P)/
Q. 知る
A. [しる] /(v5r) to know/to understand/to be acquainted with/to feel/(P)/
Q. 白い
A. [しろい] /(adj) white/(P)/
Q. ~人
A. [~じん]
Q. 新聞
A. [しんぶん] /(n) newspaper/(P)/
Q. 水曜日
A. [すいようび] /(n-adv,n) Wednesday/(P)/
Q. 吸う
A. [すう] /(v5u) to smoke/to breathe in/to suck/(P)/
Q. スカート
A. (n) skirt/(P)/
Q. 好き
A. [すき] /(adj-na,n) liking/fondness/love/(P)/
Q. すぐに
A. (adv) instantly/
Q. 少し
A. [すこし] /(adv,n) (1) small quantity/little/few/something/(2) little while/(3) short distance/(P)/
Q. 涼しい
A. [すずしい] /(adj) cool/refreshing/(P)/
Q. ストーブ
A. (n) heater (lit: stove)/(P)/
Q. スプーン
A. (n) spoon/(P)/
Q. スポーツ
A. (n) sport/(P)/
Q. ズボン
A. (fr:) (n) trousers (fr: jupon)/(P)/
Q. 住む
A. [すむ] /(v5m) to abide/to reside/to live in/to inhabit/to dwell/(P)/
Q. スリッパ
A. (n) slippers/(P)/
Q. する
A. (v5r) to pick someone's pocket/
Q. 座る
A. [すわる] /(v5r) to sit/(P)/
Q. 背
A. [せい] /(n) height/stature/(P)/
Q. 生徒
A. [せいと] /(n) pupil/(P)/
Q. セーター
A. (n) sweater/jumper/(P)/
Q. せっけん
A. (n) economy/thrift/(P)/
Q. 背広
A. [せびろ] /(n) business suit/(P)/
Q. 狭い
A. [せまい] /(adj) narrow/confined/small/(P)/
Q. ゼロ
A. (n) zero/(P)/
Q. 千
A. [せん] /thousand/many/(P)/
Q. 先月
A. [せんげつ] /(n-adv,n-t) last month/(P)/
Q. 先週
A. [せんしゅう] /(n-adv,n-t) last week/the week before/(P)/
Q. 先生
A. [せんせい] /(n) teacher/master/doctor/(P)/
Q. 洗濯
A. [せんたく] /(n,vs) washing/laundry/(P)/
Q. 全部
A. [ぜんぶ] /(n-adv,n-t) all/entire/whole/altogether/(P)/
Q. そう
A. [（そうです）] /destroy/
Q. 掃除
A. [そうじ] /(n,vs) cleaning/sweeping/(P)/
Q. そうして
A. (conj) (uk) and/like that/(P)/
Q. そして
A. (conj) (uk) and/(P)/
Q. そこ
A. [（指示詞）] /(n) bottom/sole/(P)/
Q. そちら
A. (n) (uk) over there/the other/(P)/
Q. 外
A. [そと] /(n) other place/the rest/(P)/
Q. その
A. [（連体詞）] /(adj-pn) (uk) the/that/
Q. そば
A. [（窓のそば）] /(n) near/close/beside/vicinity/proximity/besides/while/
Q. 空
A. [そら] /(n) sky/(P)/
Q. それ
A. [（指示詞）] /(n) (uk) it/that/(P)/
Q. それから
A. (uk) and then/after that/(P)/
Q. それでは
A. (exp) (uk) in that situation/well then .../(P)/
Q. ~台
A. [~だい]
Q. 大学
A. [だいがく] /(n) university/(P)/
Q. 大使館
A. [たいしかん] /(n) embassy/(P)/
Q. 大丈夫
A. [だいじょうぶ] /(adj-na,adv,n) safe/all right/O.K./(P)/
Q. 大好き
A. [だいすき] /(adj-na,n) very likeable/like very much/(P)/
Q. 大切
A. [たいせつ] /(adj-na,n) important/(P)/
Q. たいてい
A. (adj-na,adv,n) usually/generally/(P)/
Q. 台所
A. [だいどころ] /(n) kitchen/(P)/
Q. たいへん
A. [（たいへん暑い）] /(adj-na,adv,n) awful/dreadful/terrible/very/(P)/
Q. たいへん
A. [（それはたいへんですね）] /(adj-na,adv,n) awful/dreadful/terrible/very/(P)/
Q. 高い
A. [たかい] （高い山） /(adj) tall/high/expensive/(P)/
Q. たかい
A. [（値段は高い）] /(n,vs) death/the next world/
Q. たくさん
A. (adj-na,adv,n) many/a lot/much/(P)/
Q. タクシー
A. (n) taxi/(P)/
Q. 出す
A. [だす] /(v5s) to put out/to send/(P)/
Q. ~たち
Q. 立つ
A. [たつ] /(v5t) to stand/(P)/
Q. 建物
A. [たてもの] /(n) building/(P)/
Q. 楽しい
A. [たのしい] /(adj) enjoyable/fun/(P)/
Q. 頼む
A. [たのむ] /(v5m) to request/to beg/to ask/(P)/
Q. たばこ
A. (pt:) (n) (uk) tobacco (pt: tabaco)/cigarettes/
Q. たぶん
A. (adv,n) perhaps/probably/(P)/
Q. 食べ物
A. [たべもの] /(n) food/(P)/
Q. 食べる
A. [たべる] /(v1) to eat/(P)/
Q. 卵
A. [たまご] /(n) (1) egg(s)/spawn/roe/(2) (an expert) in the making/(P)/
Q. だれ
A. (n) who/(P)/
Q. 誕生日
A. [たんじょうび] /(n) birthday/(P)/
Q. だんだん
A. (adv,n) gradually/by degrees/
Q. 小さい
A. [ちいさい] /(adj) small/little/tiny/(P)/
Q. 近い
A. [ちかい] /(adj,suf) near/close by/short/(P)/
Q. 違う
A. [ちがう] /(v5u) to differ (from)/(P)/
Q. 近く
A. [ちかく] /(n-adv,n) near/neighbourhood/vicinity/(P)/
Q. 地下鉄
A. [ちかてつ] /(n) underground train/subway/(P)/
Q. 地図
A. [ちず] /(n) map/(P)/
Q. 父
A. [ちち] /(n) (hum) father/(P)/
Q. 茶色
A. [ちゃいろ] /(n) light brown/tawny/(P)/
Q. ちゃわん
A. (n) rice bowl/tea cup/teacup/
Q. ~中
A. [~ちゅう]
Q. ちょうど
A. (n) supplies/furniture/fixtures/
Q. ちょっと
A. (ateji) (adv,int) (uk) just a minute/a short time/a while/just a little/somewhat/easily/readily/rather/(P)/
Q. 一日
A. [ついたち] /(n) (1) first of month/(P)/
Q. 使う
A. [つかう] /(v5u) to use/to handle/to manipulate/to employ/to need/to want/to spend/to consume/to speak (English)/to practise (fencing)/to take (one's lunch)/to circulate (bad money)/(P)/
Q. 疲れる
A. [つかれる] /(v1) to get tired/to tire/(P)/
Q. 次
A. [つぎ] /(n) next/stage station/stage/subsequent/(P)/
Q. 着く
A. [つく] /(v5k) to arrive at/to reach/(P)/
Q. 机
A. [つくえ] /(n) desk/(P)/
Q. 作る
A. [つくる] /(v5r) to make/to create/to manufacture/to draw up/to write/to compose/to build/to coin/to cultivate/to organize/to establish/to make up (a face)/to trim (a tree)/to fabricate/to prepare (food)/to commit (sin)/to construct/(P)/
Q. つける
A. [（電灯をつける）] /(v1,vt) (1) to attach/to join/to add/to append/to affix/to stick/to glue/to fasten/to sew on/to apply (ointment)/(2) to furnish (a house with)/(3) to wear/to put on/(4) to keep a diary/to make an entry/(5) to appraise/to set (a price)/(6) to bring alongside/(7) to place (under guard or doctor)/(8) to follow/to shadow/(9) to load/to give (courage to)/(10) to keep (an eye on)/(11) to establish (relations or understanding)/(P)/
Q. 勤める
A. [つとめる] /(v1) (1) to serve/to fill a post/to serve under/to work (for)/(2) to exert oneself/to endeavor/to be diligent/(3) to play (the part of)/(P)/
Q. つまらない
A. (adj) (uk) insignificant/boring/trifling/(P)/
Q. 冷たい
A. [つめたい] /(adj) cold (to the touch)/chilly/icy/freezing/coldhearted/(P)/
Q. 強い
A. [つよい] /(adj) strong/powerful/mighty/potent/(P)/
Q. 手
A. [て] /(n) hand/(P)/
Q. テープ
A. (n) tape/(P)/
Q. テープレコーダー
A. (n) tape recorder/(P)/
Q. テーブル
A. (n) table/(P)/
Q. 出かける
A. [でかける] /(v1) to depart/to go out (e.g. on an excursion or outing)/to set out/to start/to be going out/
Q. 手紙
A. [てがみ] /(n) letter/(P)/
Q. できる
A. [（英語ができる）] /(v1) (uk) to be able to/to be ready/to occur/(P)/
Q. 出口
A. [でぐち] /(n) exit/gateway/way out/outlet/leak/vent/(P)/
Q. テスト
A. (n,vs) test/(P)/
Q. では
A. [（感動詞）] /(n) chance of going out/opportunity (to succeed)/moment of departure/beginning of work/
Q. デパート
A. (n) (abbr) department store/(P)/
Q. でも
A. (conj,prt) but/however/
Q. 出る
A. [でる] /(v1) to appear/to come forth/to leave/(P)/
Q. テレビ
A. (n) television/TV/(P)/
Q. 天気
A. [てんき] /(n) weather/the elements/fine weather/(P)/
Q. 電気
A. [でんき] /(n) electricity/(electric) light/(P)/
Q. 電車
A. [でんしゃ] /(n) electric train/(P)/
Q. 電話
A. [でんわ] /(n,vs) telephone/(P)/
Q. 戸
A. [と] /(n) door (Japanese style)/(P)/
Q. ~度
A. [~ど]
Q. ドア
A. (n) door (Western style)/(P)/
Q. トイレ
A. (n) toilet/restroom/bathroom/lavatory/(P)/
Q. どう
A. [（副詞）] /child/servant/foolishness/
Q. どうして
A. (adv,int) (uk) why?/for what reason/how/in what way/for what purpose/what for/(P)/
Q. どうぞ
A. (adv) please/kindly/by all means/(P)/
Q. 動物
A. [どうぶつ] /(n) animal/(P)/
Q. どうも
A. (adv,int) (abbr) thanks/how/(very) much/very/quite/really/somehow/no matter how hard one may try/(P)/
Q. 十
A. [とお] /(num) 10/ten/(P)/
Q. 遠い
A. [とおい] /(adj) far/distant/(P)/
Q. 十日
A. [とおか] /ten days/the tenth (day of the month)/(P)/
Q. 時々
A. [ときどき] /(adv,n) sometimes/(P)/
Q. 時計
A. [とけい] /(n) watch/clock/(P)/
Q. どこ
A. (n) (uk) where/what place/(P)/
Q. 所
A. [ところ] /(n) place/(P)/
Q. 図書館
A. [としょかん] /(n) library/(P)/
Q. どちら
A. (n) (uk) which (of two)/who/(P)/
Q. とても
A. (adv) (uk) very/awfully/exceedingly/(P)/
Q. どなた
A. (n) (uk) who?/(P)/
Q. 隣
A. [となり] /(n) next to/next door to/(P)/
Q. どの
A. [（どの人）] /(pol) person/Mister (mostly in addressing someone on an envelope)/Mr/(P)/
Q. 飛ぶ
A. [とぶ] /(v5b) to jump/to fly/to leap/to spring/to bound/to hop/(P)/
Q. 止まる
A. [とまる] /(v5r) to come to a halt/(P)/
Q. 友達
A. [ともだち] /(n) friend/(P)/
Q. 土曜日
A. [どようび] /(n-adv,n) Saturday/(P)/
Q. 鳥
A. [とり] /(n) bird/fowl/poultry/(P)/
Q. 鳥肉
A. [とりにく] /(n) chicken meat/
Q. 取る
A. [とる] /(v5r) to take/to pick up/to harvest/to earn/to choose/(P)/
Q. 撮る
A. [とる] /(v5r) to take (a photo)/to make (a film)/(P)/
Q. どれ
A. (n) (uk) well/now/let me see/which (of three or more)/(P)/
Q. どんな
A. (adj-na,adj-pn,n) what/what kind of/(P)/
Q. ない
A. (adj) there isn't/doesn't have/(P)/
Q. ナイフ
A. (n) knife/(P)/
Q. 中
A. [なか] /(n) inside/middle/among/(P)/
Q. 長い
A. [ながい] /(adj) long/(P)/
Q. 鳴く
A. [なく] /(v5k) to bark/to purr/to make sound (animal)/(P)/
Q. 夏
A. [なつ] /(n-adv,n-t) summer/(P)/
Q. 夏休み
A. [なつやすみ] /(n) summer vacation/summer holiday/(P)/
Q. 七つ
A. [ななつ] /(n) seven/(P)/
Q. 何
A. [なに] /(int,n) what/(P)/
Q. 七日
A. [なのか] /(n-adv) seven days/the seventh day (of the month)/(P)/
Q. 名前
A. [なまえ] /(n) name/(P)/
Q. 習う
A. [ならう] /(v5u) to learn/(P)/
Q. 並ぶ
A. [ならぶ] /(v5b,vi) to line up/to stand in a line/(P)/
Q. 並べる
A. [ならべる] /(v1,vt) to line up/to set up/(P)/
Q. なる
A. [（春になる）] /(v5r) to sound/to ring/to resound/to echo/to roar/to rumble/(P)/
Q. 二
A. [に] /(num) two/(P)/
Q. にぎやか
A. (adj-na,n) bustling/busy/(P)/
Q. 肉
A. [にく] /(n) meat/(P)/
Q. 西
A. [にし] /(n) west/(P)/
Q. ~日
A. [~にち]
Q. 日曜日
A. [にちようび] /(n-adv,n) Sunday/(P)/
Q. 荷物
A. [にもつ] /(n) luggage/(P)/
Q. ニュース
A. (n) news/(P)/
Q. 庭
A. [にわ] /(n) garden/(P)/
Q. ~人
A. [~にん]
Q. 脱ぐ
A. [ぬぐ] /(v5g) to take off clothes/(P)/
Q. ネクタイ
A. (n) tie/necktie/(P)/
Q. 寝る
A. [ねる] /(v1) to go to bed/to lie down/to sleep/(P)/
Q. ~年
A. [~ねん]
Q. ノート
A. (n) notebook/copy-book/exercise book/(P)/
Q. 登る
A. [のぼる] /(v5r) to climb/(P)/
Q. 飲み物
A. [のみもの] /(n) drink/beverage/(P)/
Q. 飲む
A. [のむ] /(v5m) to drink/(P)/
Q. 乗る
A. [のる] /(v5r) (1) to get on/to ride in/to board/to mount/to get up on/to spread (paints)/to be taken in/(2) to share in/to join/to be found in (a dictionary)/to feel like doing/to be mentioned in/to be in harmony with/(P)/
Q. 歯
A. [は] /(n) tooth/(P)/
Q. パーティー
A. (n) party/(P)/
Q. はい
A. [（感動詞）] /wear/put on (sword)/
Q. ~杯
A. [~はい]
Q. 灰皿
A. [はいざら] /(n) ashtray/(P)/
Q. 入る
A. [はいる] /(v5r) to enter/to break into/to join/to enroll/to contain/to hold/to accommodate/to have (an income of)/(P)/
Q. 葉書
A. [はがき] /(n) postcard/(P)/
Q. はく
A. [（ズボンをはく、] 靴をはく） /(v5k) to wear/to put on (a sword)/
Q. 箱
A. [はこ] /(n) box/(P)/
Q. 橋
A. [はし] /(n) bridge/(P)/
Q. はし
A. [（はしで御飯を食べる）] /(n) chopsticks/(P)/
Q. 始まる
A. [はじまる] /(v5r,vi) to begin/(P)/
Q. 初めに
A. [はじめに]
Q. 初めて
A. [はじめて] /(adv,n) for the first time/(P)/
Q. 走る
A. [はしる] /(v5r) to run/(P)/
Q. バス
A. (n) (1) bus/(2) bath/(3) bass/(P)/
Q. バター
A. (n) butter/(P)/
Q. 二十歳
A. [はたち] /(n) 20 years old/20th year/(P)/
Q. 働く
A. [はたらく] /(v5k) to work/to labor/to do/to act/to commit/to practise/to work on/to come into play/to be conjugated/to reduce the price/(P)/
Q. 八
A. [はち] /(num) eight/
Q. 二十日
A. [はつか] /(n) twenty days/twentieth (day of the month)/(P)/
Q. 花
A. [はな] /(n) flower/(P)/
Q. 鼻
A. [はな] /(n) nose/(P)/
Q. 話
A. [はなし] /(io) (n) talk/speech/chat/story/conversation/(P)/
Q. 話す
A. [はなす] /(v5s) to speak/(P)/
Q. 母
A. [はは] /(n) (hum) mother/(P)/
Q. 早い
A. [はやい] /(adj) early/(P)/
Q. 速い
A. [はやい] /(adj) quick/fast/swift/(P)/
Q. 春
A. [はる] /(n-adv,n-t) spring/(P)/
Q. はる
A. [（切手をはる）] /(v5r) to stick/to paste/(P)/
Q. 晴れる
A. [はれる] /(v1) to be sunny/to clear away/to stop raining/(P)/
Q. 半
A. [はん] /(n-adv,n,n-suf) half/(P)/
Q. 晩
A. [ばん] /(n-adv,n-t) evening/(P)/
Q. ~番
A. [~ばん]
Q. パン
A. (pt:) (n) (1) bread (pt: pan)/(2) panning/(P)/
Q. ハンカチ
A. (n) handkerchief/(P)/
Q. 番号
A. [ばんごう] /(n) number/series of digits/(P)/
Q. 晩御飯
A. [ばんごはん] /(n) dinner/evening meal/
Q. 半分
A. [はんぶん] /half minute/
Q. 東
A. [ひがし] /(n) east/(P)/
Q. ~匹
A. [~ひき]
Q. 引く
A. [ひく] /(v5k) minus/to pull/to play (string instr.)/(P)/
Q. 弾く
A. [ひく] /(v5k) to play (piano, guitar)/(P)/
Q. 低い
A. [ひくい] /(adj) short/low/humble/low (voice)/(P)/
Q. 飛行機
A. [ひこうき] /(n) aeroplane/(P)/
Q. 左
A. [ひだり] /(n) left hand side/(P)/
Q. 人
A. [ひと] /(n) man/person/human being/mankind/people/character/personality/true man/man of talent/adult/other people/messenger/visitor/(P)/
Q. 一つ
A. [ひとつ] /(n) one/(P)/
Q. 一月
A. [ひとつき] /one month/(P)/
Q. 一人
A. [ひとり] /(n) one person/(P)/
Q. 暇
A. [ひま] /(adj-na,n) (1) free time/leisure/leave/spare time/(2) farewell/
Q. 百
A. [ひゃく] /(num) 100/hundred/(P)/
Q. 病院
A. [びょういん] /(n) hospital/(P)/
Q. 病気
A. [びょうき] /(n) illness/disease/sickness/(P)/
Q. 平仮名
A. [ひらがな] /(n) hiragana/47 syllables/the cursive syllabary/(P)/
Q. 昼
A. [ひる] /(n-adv,n-t) noon/daytime/(P)/
Q. 昼御飯
A. [ひるごはん] /(n) lunch/midday meal/
Q. 広い
A. [ひろい] /(adj) spacious/vast/wide/(P)/
Q. フィルム
A. (n) film (roll of)/(P)/
Q. 封筒
A. [ふうとう] /(n) envelope/(P)/
Q. プール
A. (n) swimming pool/(P)/
Q. フォーク
A. (n) folk/fork/(P)/
Q. 吹く
A. [ふく] /(v5k) (1) to blow (wind, etc)/(2) to emit/to spout/(P)/
Q. 服
A. [ふく] /(n,n-suf) clothes/(P)/
Q. 二つ
A. [ふたつ] /(n) two/(P)/
Q. 豚肉
A. [ぶたにく] /(n) pork/(P)/
Q. 二人
A. [ふたり] /(n) two persons/two people/pair/couple/(P)/
Q. 二日
A. [ふつか] /(n) second day of the month/two days/(P)/
Q. 太い
A. [ふとい] /(adj) fat/thick/(P)/
Q. 冬
A. [ふゆ] /(n-adv,n-t) winter/(P)/
Q. 降る
A. [ふる] /(v5r) to precipitate/to fall (e.g. rain)/(P)/
Q. 古い
A. [ふるい] /(adj) old (not person)/aged/ancient/antiquated/stale/threadbare/outmoded/obsolete article/(P)/
Q. ふろ
A. (n) bath/(P)/
Q. ~分
A. [~ふん]
Q. パージ
A. (n) purge/(P)/
Q. 下手
A. [へた] /(adj-na,n) unskillful/poor/awkward/(P)/
Q. ベッド
A. (n) bed/(P)/
Q. 部屋
A. [へや] /(n) room/(P)/
Q. 辺
A. [へん] /(n) area/vicinity/(P)/
Q. ペン
A. (n) pen/P.E.N. (club)/(P)/
Q. 勉強
A. [べんきょう] /(n,vs) study/diligence/discount/reduction/(P)/
Q. 便利
A. [べんり] /(adj-na,n) convenient/handy/useful/(P)/
Q. ほう
A. [（比較を表す）] /divide/
Q. 帽子
A. [ぼうし] /(n) hat/(P)/
Q. ボールペン
A. (n) ball-point pen/(P)/
Q. 外
A. [ほか] /(n) other place/the rest/(P)/
Q. ポケット
A. (n) pocket/(P)/
Q. 欲しい
A. [ほしい] /(adj) wanted/wished for/in need of/desired/(P)/
Q. 細い
A. [ほそい] /(adj) thin/slender/fine/(P)/
Q. ボタン
A. (pt:) (n) button (pt: bota~o)/(P)/
Q. ホテル
A. (n) hotel/(P)/
Q. 本
A. [ほん] /(n,n-suf,n-t) (1) origin/original/(P)/
Q. ~本
A. [~ほん]
Q. 本棚
A. [ほんだな] /(n) bookshelves/(P)/
Q. ほんとうに
A. (adv) really/truly/(P)/
Q. ~枚
A. [~まい]
Q. 毎朝
A. [まいあさ] /(n-adv,n-t) every morning/
Q. 毎月
A. [まいげつ] /(n-adv,n) every month/each month/monthly/(P)/
Q. 毎月
A. [まいつき] /(n-adv,n) every month/each month/monthly/(P)/
Q. 毎週
A. [まいしゅう] /(n-adv,n-t) every week/(P)/
Q. 毎日
A. [まいにち] /(n-adv,n-t) every day/(P)/
Q. 毎年
A. [まいねん] /(n-t) every year/yearly/annually/(P)/
Q. 毎年
A. [まいとし] /(n-t) every year/yearly/annually/(P)/
Q. 毎晩
A. [まいばん] /(n-adv,n-t) every night/(P)/
Q. 前
A. [まえ] /(n-adv,n-t,suf) (1) before/in front/fore part/ago/previously/(2) head (of a line)/(3) in the presence of/lady (so-and-so)/(4) (five minutes) to/(5) helping/portion/(P)/
Q. ~前
A. [~まえ]
Q. 曲る
A. [まがる]
Q. まずい
A. (adj) (uk) unappetising/unpleasant (taste, appearance, situation)/ugly/unskilful/awkward/bungling/unwise/untimely/(P)/
Q. また
A. (adv,conj,n) again/and/(P)/
Q. まだ
A. (adv) yet/still/more/besides/(P)/
Q. 町
A. [まち] /(n) (1) town/(2) street/road/(P)/
Q. 待つ
A. [まつ] /(v5t) to wait/(P)/
Q. まっすぐ
A. (adj-na,adv,n) straight (ahead)/direct/upright/erect/honest/frank/
Q. マッチ
A. (n) match/(P)/
Q. 窓
A. [まど] /(n) window/(P)/
Q. 丸い
A. [まるい] /(adj) round/circular/spherical/(P)/
Q. 円い
A. [まるい] /(adj) round/circular/spherical/(P)/
Q. 万
A. [まん] /(adv,num) 10,000/ten thousand/myriads/all/everything/
Q. 万年筆
A. [まんねんひつ] /(n) fountain pen/(P)/
Q. 磨く
A. [みがく] /(v5k) to polish/to shine/to brush/to refine/to improve/(P)/
Q. 右
A. [みぎ] /(n) right hand side/(P)/
Q. 短い
A. [みじかい] /(adj) short/(P)/
Q. 水
A. [みず] /(n) water/(P)/
Q. 店
A. [みせ] /(n,n-suf) store/shop/establishment/(P)/
Q. 見せる
A. [みせる] /(v1) to show/to display/(P)/
Q. 道
A. [みち] /(n) road/street/way/method/(P)/
Q. 三日
A. [みっか] /(n) three days/the third day (of the month)/(P)/
Q. 三つ
A. [みっつ] /(n) three/(P)/
Q. 皆さん
A. [みなさん] /(n) everyone/(P)/
Q. 南
A. [みなみ] /(n,vs) South/proceeding south/(P)/
Q. 耳
A. [みみ] /(n) ear/(P)/
Q. 見る
A. [みる] /(v1) to see/to watch/(P)/
Q. みんな
A. (adv,n) all/everyone/everybody/(P)/
Q. 六日
A. [むいか] /six days/sixth (day of month)/(P)/
Q. 向こう
A. [むこう] /(n) beyond/over there/opposite direction/the other party/(P)/
Q. 難しい
A. [むずかしい] /(adj) difficult/(P)/
Q. 六つ
A. [むっつ] /(num) six/
Q. 目
A. [め] /(n) eye/eyeball/(P)/
Q. メートル
A. metre (meter) (old form)/
Q. 眼鏡
A. [めがね] /(n) spectacles/glasses/(P)/
Q. もう
A. [（もう、帰りました）] /net/
Q. もう
A. [（もう一度）] /net/
Q. 木曜日
A. [もくようび] /(n-adv,n) Thursday/(P)/
Q. もしもし
A. (conj,int) hello (on phone)/(P)/
Q. もちろん
A. (adv) (uk) of course/certainly/naturally/(P)/
Q. 持つ
A. [もつ] /(v5t) (1) to hold/to carry/(2) to possess/(P)/
Q. もっと
A. (adv) more/longer/farther/(P)/
Q. 物
A. [もの] /(n) thing/object/(P)/
Q. 門
A. [もん] /(n,n-suf) gate/(P)/
Q. 問題
A. [もんだい] /(n) problem/question/(P)/
Q. ~屋
A. [~や]
Q. 八百屋
A. [やおや] /(n) greengrocer/(P)/
Q. 野菜
A. [やさい] /(n) vegetable/(P)/
Q. 易しい
A. [やさしい] /(adj) easy/plain/simple/(P)/
Q. 安い
A. [やすい] /(adj) cheap/inexpensive/peaceful/quiet/gossipy/thoughtless/(P)/
Q. 休み
A. [やすみ] /(n) (1) rest/recess/respite/(2) vacation/holiday/absence/suspension/(3) moulting/(P)/
Q. 休む
A. [やすむ] /(v5m,vi) to rest/to have a break/to take a day off/to be finished/to be absent/to retire/to sleep/(P)/
Q. 八つ
A. [やっつ] /(num) eight/
Q. 山
A. [やま] /(n) (1) mountain/(2) pile/heap/(3) climax/critical point/(P)/
Q. やる
A. [（仕事をやる）] /(v5r) (col) (uk) to do/to have sexual intercourse/to kill/to give (to inferiors, animals, etc.)/to dispatch (a letter)/to send/to study/to perform/to play (sports, game)/to have (eat, drink, smoke)/to row (a boat)/to run or operate (a restaurant)/
Q. 夕方
A. [ゆうがた] /(n-adv,n-t) evening/(P)/
Q. 郵便局
A. [ゆうびんきょく] /(n) post office/(P)/
Q. ゆうべ
A. [（’昨晩’の意）] /(n) evening/
Q. 有名
A. [ゆうめい] /(adj-na,n) fame/(P)/
Q. 雪
A. [ゆき] /(n) snow/(P)/
Q. ゆっくり
A. (adv,n) slowly/at ease/(P)/
Q. 八日
A. [ようか] /(n) eight days/the eighth (day of the month)/(P)/
Q. 洋服
A. [ようふく] /(n) Western-style clothes/(P)/
Q. よく
A. [（ようく行きます）] /(adv,n,vs) nicely/properly/well/skilled in/
Q. よく
A. [（よくできました）] /(adv,n,vs) nicely/properly/well/skilled in/
Q. 横
A. [よこ] /(n) beside/side/width/(P)/
Q. 四日
A. [よっか] /(n) (1) 4th day of month/(2) four days/(P)/
Q. 四つ
A. [よっつ] /(n) four/(P)/
Q. 呼ぶ
A. [よぶ] /(v5b) to call out/to invite/(P)/
Q. 読む
A. [よむ] /(v5m) to read/(P)/
Q. 夜
A. [よる] /(n-adv,n-t) evening/night/(P)/
Q. 来月
A. [らいげつ] /(n-adv,n-t) next month/(P)/
Q. 来週
A. [らいしゅう] /(n-adv,n-t) next week/(P)/
Q. 来年
A. [らいねん] /(n-adv,n-t) next year/(P)/
Q. ラジオ
A. (n) radio/(P)/
Q. りっぱ
A. (adj-na,n) splendid/fine/handsome/elegant/imposing/prominent/legal/legitimate/(P)/
Q. 留学生
A. [りゅうがくせい] /(n) overseas student/(P)/
Q. 両親
A. [りょうしん] /(n) parents/both parents/(P)/
Q. 料理
A. [りょうり] /(n,vs) cooking/cookery/cuisine/(P)/
Q. 旅行
A. [りょこう] /(n,vs) travel/trip/(P)/
Q. 零
A. [れい] /(n) zero/nought/(P)/
Q. 冷蔵庫
A. [れいぞうこ] /(n) refrigerator/(P)/
Q. レコード
A. (n) record/(P)/
Q. レストラン
A. (n) restaurant/(P)/
Q. 練習
A. [れんしゅう] /(n,vs) practice/(P)/
Q. 六
A. [ろく] /(num) six/(P)/
Q. ワイシャツ
A. (n) shirt (lit: white shirt)/business shirt/(P)/
Q. 若い
A. [わかい] /(adj) young/(P)/
Q. 分る
A. [わかる] /(io) (v5r) to be understood/
Q. 忘れる
A. [わすれる] /(v1) to forget/to leave carelessly/to be forgetful of/to forget about/to forget (an article)/(P)/
Q. 私
A. [わたくし] /(adj-no,n) I/myself/private affairs/(P)/
Q. わたし
A. (n) ferry (crossing)/ferry(boat)/(also suffix) delivery/
Q. 渡す
A. [わたす] /(v5s) to pass over/to hand over/(P)/
Q. 渡る
A. [わたる] /(v5r) to cross over/to go across/(P)/
Q. 悪い
A. [わるい] /(adj) bad/inferior/(P)/
