#!/bin/sh
# usage: sh autogen.sh

actually ()
{
    gnulib-tool --copy-file $1 $2
}
actually build-aux/install-sh install-sh
actually doc/INSTALL.UTF-8 INSTALL

autoconf

# autogen.sh ends here
