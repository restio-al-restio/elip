Welcome
-------

	This is ELIP, the Emacs Learning Instruction Program.
	You can use it to improve long-term retention (memory)
	of arbitrary sets of associated data.

	ELIP was originally (up through 0.412beta) written by Bob Newell,
	and subsequently (0.801 and later) rewritten by Thien-Thi Nguyen.


Requirements
------------

	Emacs 22 or later -- http://www.gnu.org/software/emacs/
	EDB 1.31 or later -- http://www.gnuvola.org/software/edb/


Installation
------------

	See INSTALL for generic instructions; use "./configure --help"
	to see all options.  Notably, ELIP requires EDB to be installed
	at "make" time as well as at runtime.  The configure script expects
	EDB to be somewhere under the "site lisp" directory.  If your site
	is different, use the option --with-edb-installation=DIR, where DIR
	is the directory where EDB (database.el et al) can be found.

	Upon installation, $(sitelisp) will have elip.el and elip.elc, and
	$(infodir) will have elip.info.  Presuming $(sitelisp) is in your
	‘load-path’, you can now add to ~/.emacs the form:

	(require 'elip)

	to make ELIP available on next restart.  See manual for more info.


Problems
--------

	First, please see: (info "(elip) Don't panic!")

	If that doesn't help, contact <ttn@gnuvola.org>.
