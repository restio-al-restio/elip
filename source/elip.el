;;; elip.el

;; Copyright (C) 2008-2021 Thien-Thi Nguyen
;;
;; This file is part of ELIP.
;;
;; ELIP is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3, or (at your option) any later
;; version.
;;
;; ELIP is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with ELIP.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(eval-when-compile (require 'cl))
(eval-when-compile (require 'view))
(require 'calendar)
(require 'database)
(require 'edb-t-timedate1)

;; snarfed from EDB 1.32 lisp/system.el
(unless (require 'cl-lib nil t)
  ;; Add other ‘cl-lib’ time travellers here.
  (defalias 'cl-loop 'loop)
  (defalias 'cl-labels 'labels)
  ;; The following are arguably sick and wrong.  So it goes.
  (defalias 'cl-flet* 'labels)
  (defalias 'cl-flet 'flet))

(defconst elip-version "DEVELOPMENT")

(defconst elip-cycle-days [4 7 12 20 30 61 92 153 276
                             488 731 1462 2193 4018 6574]
  "Days for each cycle in interval learning mode.")

(defconst elip-text-decrement 10)

(defconst elip-control-properties
  `(:fields
    [(question . string)
     (answer . string)
     (nextask . date)
     (cycle . integer)
     (lscore . integer)
     (trials . integer)
     (tscore . integer)
     (ttime . integer)
     (lmissed . integer)
     (lhole . integer)
     (lnew . integer)
     (lright . integer)
     (percent . integer)]
    :record-defaults
    'elip-new-record-plist))

;; All database-local variables:
;; * max-new 20
;;   Maximum number of new/old items to learn at once.
;;   Don't make overly large.  If there are more than that many items,
;;   learn them in groups (which can be done in the same sitting).
;; * leitner-last-box 5
;;   This is usually 5 but some methods use a higher number.
;; * ok-score 4
;;   Score for acceptable answer.
;; * text-percent-min 20
;; * text-percent-max 90
;; * elip-learn-random t
;;   Set to nil to learn items in entry sequence rather than randomized.
;; * input-method (optional, omitted means nil)
;;   An input method name (string).

(defvar elip-known-databases (make-hash-table :weakness 'key :test 'eq)
  "Registry (weak-key hash table) of known ELIP databases.
This is used to distinguish an ELIP database from other
databases managed by EDB.")

(defvar elip-child-buffer-reckless nil
  "*If non-nil, overwrite existing child buffers without asking.")

(make-variable-buffer-local
 (defvar elip-blink nil
   "Buffer linkages internal to ELIP.
For parent buffer, a list of child buffers.
For child buffer, the parent buffer."))

(defvar elip-question-face nil
  "*A face for questions in ELIP.
If nil, ELIP uses the ‘default’ face.")

(defun elip-child-buffer (kind)
  (let* ((fancy (database-print-name dbc-database))
         (name (format "%S %s" fancy kind))
         (parent (current-buffer))
         buf)
    (if (setq buf (get-buffer name))
        (unless (or elip-child-buffer-reckless
                    (yes-or-no-p (format "%S exists -- overwrite? " buf)))
          (error "Command cancelled"))
      (with-current-buffer (setq buf (get-buffer-create name))
        (help-mode)
        (buffer-disable-undo)
        (setq buffer-read-only nil
              elip-blink parent)
        (insert " [")
        (insert-text-button
         fancy
         'action (lambda (button)
                   (if (buffer-live-p elip-blink)
                       (switch-to-buffer elip-blink)
                     (message "(Oops, %S no longer around.)"
                              (button-label button)))))
        (insert "] " (capitalize kind) "\n")))
    (with-current-buffer buf
      (setq buffer-read-only nil)
      (goto-char (point-min))
      (forward-line 1)
      (delete-region (point) (point-max)))
    (setq elip-blink (delq nil (mapcar (lambda (buf)
                                         (and (buffer-live-p buf) buf))
                                       elip-blink)))
    (push buf elip-blink)
    buf))

(defun elip-clean-up ()
  (when (consp elip-blink)
    (mapc 'kill-buffer elip-blink))
  (let ((help (when (gethash dbc-database elip-known-databases)
                (remhash dbc-database elip-known-databases)
                (get-buffer "*ELIP Help*"))))
    (when help
      (kill-buffer help))))

(defun elip-session (filename)
  (interactive "fDatabase for ELIP session: ")
  (edb-interact filename nil)
  (unless dbc-database
    (error "Could not connect to database"))
  (let ((in-meth (database-get-local 'input-method dbc-database t)))
    (when in-meth (set-input-method in-meth)))
  (setf (gethash dbc-database elip-known-databases) t)
  (add-hook 'kill-buffer-hook 'elip-clean-up nil t)
  (message "Type M-x elip-help RET for a help screen"))

(defun elip-current-date (&optional return-improper-p)
  (if return-improper-p
      (destructuring-bind (m d y)
          (calendar-current-date)
        (list* y m d))
    (calendar-absolute-from-gregorian
     (calendar-current-date))))

(defun elip-new-record-plist ()
  (list 'nextask (elip-current-date t)
        'cycle 0
        'lscore 0
        'trials 0
        'tscore 0
        'ttime 0
        'lmissed 0
        'lhole 0
        'lnew 0
        'lright 0
        'percent 90))

(defun elip-maprecords-with-index (func)
  (lexical-let ((func func)
                (i 0))
    (catch 'elip-maprecords-done
      (db-maprecords
       (lambda (record)
         (funcall func record (incf i)))))))

(defun elip-maprecords-with-index-until (func)
  (lexical-let ((func func))
    (elip-maprecords-with-index
     (lambda (record index)
       (when (funcall func record index)
         (throw 'elip-maprecords-done t))))))

(defun elip-hey (s &rest args)
  (read-from-minibuffer
   (concat (apply 'format s args) " (press ENTER to continue)")))

(defun elip-check-db (&optional writeable-p)
  (unless dbc-database
    (if (buffer-live-p elip-blink)
        (set-buffer elip-blink)
      (error "You are not in an ELIP database buffer")))
  (unless (gethash dbc-database elip-known-databases)
    (error "This is not an ELIP database buffer"))
  (when writeable-p
    (let ((fn (database-file dbc-database)))
      (unless (file-writable-p fn)
        (error "%s not writeable" fn)))))

(defun elip-shuffle-vector (vector)
  "Randomly permute the elements of VECTOR.
Note: This function is adapted from cookie1.el ‘shuffle-vector’."
  (let ((len (length vector)))
    (dotimes (i len)
      (rotatef (aref vector (+ i (random (- len i))))
               (aref vector i)))))

(defun elip-find-nextask (record)
  "Find the next date to ask this question"
  ;; This is called from a non-ELIP buffer, beware
  (let ((cyci (db-record-field record 'cycle))
        (due-date (elip-current-date))
        adder df)
    ;;
    ;; find date adder
    ;;
    ;; this algorithm can and will be tinkered with a lot
    ;; it must depend on cycle but can also depend on item difficulty
    ;; and whether and how many times we missed it this round
    ;;
    ;; it's very loosely based on the Supermemo Algorithm 2
    ;; but I decided to try a few ideas out
    ;;
    ;; the major departure is using times correct and times wrong
    ;; rather than the grades themselves.  I don't think the
    ;; grades are accurate enough.
    ;;
    ;; as of 2003-10-21:
    ;;   we start with the base adder for the cycle
    ;;   if we have any misses, and cycle=0 make it 1 day
    ;;   otherwise we decrease time based on difficulty factor.
    ;;
    ;;   for instance if up to now we have 50% accuracy on a given
    ;;   item we decrease the interval by approx. 50% for the next
    ;;   cycle.  this doesn't favor recent learning over total
    ;;   learning, perhaps it should, but it does mean we are
    ;;   hammering those questions that we miss the most.
    ;;
    ;;   if a question is 'flubbed' with a score of 0-2
    ;;   (based on a passing grade of 4) we reset the cycle
    ;;   to the beginning, although we still keep the rest of
    ;;   the stats.  the idea is that you clearly need more
    ;;   frequent repetition but the difficulty of the question
    ;;   should be rated about the same
    ;;
    ;;   another idea is to grant a first-day exception, i.e.,
    ;;   since we already force a next-day repeat on anything we
    ;;   miss the first learning cycle, maybe we shouldn't count
    ;;   a potentially large number of first-cycle misses, which
    ;;   will bias the repeats for a very long time.  or maybe
    ;;   we should bias in this manner.  any ideas?
    ;;   what about randomization?  to spread the load should we
    ;;   be doing a percentage randomization?
    ;;
    (setq cyci (max 0 (1- (min (length elip-cycle-days) cyci))))
    (setq adder (aref elip-cycle-days cyci))
    ;;
    ;; exceptions -- order matters!
    ;; difficultly factor decreases repetition interval
    ;;
    (setq df (/ (1+ (float cyci))
                (float (db-record-field record 'trials))))
    (setq adder (truncate (* (float adder) df)))
    ;;
    ;; first cycle with misses - must come last in exceptions sequence
    ;; calls for a next day repeat, not a 4 day repeat
    ;;
    (when (and
           (zerop cyci)
           (> (db-record-field record 'lmissed)
              0))
      (setq adder 1))
    ;; add to elip-temp-date fix format and return it
    (when (< adder 1)
      (setq adder 1))
    (setq due-date (+ due-date adder))
    ;; we must return the EDB formatted date
    (edb-t-timedate1:parse-date-string
     (calendar-date-string (calendar-gregorian-from-absolute due-date)))))

(defun elip-check-for-code ()
  "In the buffer check for code to evaluate headed by ##### marker."
  ;;
  ;; Anything below the line "#####" is taken and evaluated as Elisp.
  ;; Beware, it is easy to make mistakes here.  This however makes possible
  ;; graphics, sound files, and much more.  Not at all well tested.

  ;; Bad design point: this works if there is stuff in both the Q and A
  ;; but only through sheer luck.  That's because we delete the stuff from
  ;; the question after we use it.  Could really be done better, yah?

  ;; called from non-ELIP buffer

  (goto-char (point-min))
  (when (search-forward-regexp "^#####$" nil t)
    (let* ((start (match-end 0))
           (filename (buffer-file-name))
           (varname (concat filename ".elisp-check"))
           (rcfile "~/.eliprc"))
      ;; Look for intern variable with special name; read if there, otherwise
      ;; do the whole proc.  This is so we only check once per session per file
      ;; for permission to run.
      (unless (intern-soft varname)
        ;; no var, do the whole works
        ;; create the var as nil
        (put (intern varname) 'elisp-check nil)
        (with-temp-file rcfile
          (when (file-exists-p rcfile)
            (insert-file-contents rcfile))
          (if (search-forward (concat filename " OK") nil t)
              (put (intern-soft varname)
                   'elisp-check t)
            ;; else check not ok
            (unless (search-forward (concat filename " NOT OK") nil 1)
              (insert filename " ")
              ;; else ask user
              (if (yes-or-no-p
                   "Elisp code found.  Trust this database and run it? ")
                  (put (intern-soft varname) 'elisp-check t)
                (insert "NOT "))
              (insert "OK\n")))))
      (when (get (intern-soft varname) 'elisp-check)
        ;; When we run asynch code, we get an extra half-window about the code,
        ;; and when it completes, we overwrite our message in the minibuffer
        ;; about answering the question.  Don't have a solution yet for this.
        (eval-region start (point-max)))
      ;; don't display the code in the question/answer buffer
      ;; or the hashmarks (6 = 5 + newline) either for that matter
      (delete-region (- start 6) (point-max)))))

(defun elip-learn-list (operation-mode lls)
  "Ask, in OPERATION-MODE (a symbol), the list of learning items LLS.
OPERATION-MODE is one of: ‘flashcard’, ‘interval’, ‘leitner’, ‘text’."
  (elip-check-db t)
  ;; mark the database modified immediately in case of "break out"
  (unless (eq 'flashcard operation-mode)
    (database-set-modified-p dbc-database t))
  (let ((ok-score (database-get-local 'ok-score dbc-database))
        (pct-min (database-get-local 'text-percent-min dbc-database))
        (pct-max (database-get-local 'text-percent-max dbc-database))
        (in-meth (database-get-local 'input-method dbc-database t))
        (text-p (eq 'text operation-mode))
        (pbuf (current-buffer))
        (abuf-name (concat "*" (buffer-name) ".ask*"))
        bad serial ovs q-start typed time-used qscore flubp)
    ;; set current pass counter to 0
    (let ((indices (sort lls '<)))
      (setq lls nil)
      (elip-maprecords-with-index-until
       (lambda (record index)
         (when (= index (car indices))
           (pop indices)
           (push record lls)
           (db-record-set-field record 'lmissed 0))
         (null indices)))
      (setq lls (nreverse lls)))
    ;; main loop until all questions correct
    (while lls
      (setq bad nil serial 0)
      ;; randomize if desired
      (when (equal t (database-get-local 'elip-learn-random dbc-database t))
        (let ((v (apply 'vector lls)))
          (elip-shuffle-vector v)
          (setq lls (append v nil))))
      ;; loop over all remaining questions
      (dolist (record lls)
        (cl-flet
            ((nref (f) (with-current-buffer pbuf
                         (db-record-field record f)))
             (nset (&rest plist) (with-current-buffer pbuf
                                   (while plist
                                     (let ((f (pop plist))
                                           (v (pop plist)))
                                       (db-record-set-field
                                        record f v))))))
          ;; ask the question
          (when (get-buffer abuf-name)
            (kill-buffer abuf-name))
          (switch-to-buffer (get-buffer-create abuf-name))
          (when in-meth
            (set-input-method in-meth))
          (delete-other-windows)
          (insert "\n**** " (if text-p "Text" "Question") " ****\n\n")
          (save-excursion (insert (propertize
                                   (nref 'question)
                                   'face (or elip-question-face 'default))))
          (when text-p
            ;; underscores
            (let ((percent (nref 'percent)))
              (while (re-search-forward "\\sw+" nil 1)
                (when (<= percent (random 100))
                  (let ((b (match-beginning 0))
                        (e (match-end 0)))
                    (put-text-property b e 'face 'font-lock-constant-face)
                    (let ((ov (make-overlay b e)))
                      (overlay-put ov 'display (make-string (- e b) ?_))
                      (push ov ovs)))))))
          (elip-check-for-code)
          (setq q-start (current-time))
          (unless (let ((inhibit-quit t))
                    (condition-case nil
                        (setq
                         typed
                         (read-from-minibuffer
                          (format "%d of %d%s.  Answer (type %s to quit): "
                                  (incf serial) (length lls)
                                  (case operation-mode
                                    ((leitner)
                                     (format ", slot %d" (nref 'lhole)))
                                    ((text)
                                     (format ", %d%%" (nref 'percent)))
                                    (t ""))
                                  (substitute-command-keys
                                   "\\[keyboard-quit]"))
                          nil nil nil nil nil
                          'inherit-input-method))
                      (quit nil)))
            (switch-to-buffer pbuf)
            (kill-buffer abuf-name)
            (database-set-modified-p dbc-database t)
            (db-save-database)
            (db-next-record 0)
            (error "Quit learning session at student request, database saved"))
          ;; don't combine - keep measurement error as small as possible
          ;; but ensure 1 second 'minimum'
          (setq time-used (max 1 (truncate (time-to-seconds
                                            (time-subtract (current-time)
                                                           q-start)))))
          (goto-char (point-max))
          (if text-p
              (mapc 'delete-overlay ovs)
            (insert "\n\n**** Answer ****\n\n" (nref 'answer))
            (unless (string= "" typed)
              (insert "\n\n**** Your Answer ****\n\n" typed)))
          (elip-check-for-code)
          (setq qscore -1)
          (while (or
                  (> qscore 5)
                  (< qscore 0))
            (setq qscore (read-number "Enter score, 0 to 5, and press ENTER: ")))
          (setq flubp (< qscore (1- ok-score)))
          ;; get out of foreign buffer at earliest moment possible
          (switch-to-buffer pbuf)
          (kill-buffer abuf-name)
          ;; don't record time until the answer is 'official' to avoid problems
          ;; of bad stats when quitting in the middle of a question
          ;; and don't do any of this in flashcards mode
          ;; these are the stats that don't depend on right or wrong answer
          ;; and are set for leitner/non-leitner both
          (unless (eq 'flashcard operation-mode)
            ;; first do the general stuff
            (nset 'ttime (+ time-used (nref 'ttime))
                  'lscore qscore
                  'trials (1+ (nref 'trials))
                  'tscore (+ qscore (nref 'tscore)))
            (when (eq 'interval operation-mode)
              ;; interval mode - question flubbed so badly we
              ;; start over?  set cycle back to 0, it's almost
              ;; like a new question
              (when flubp
                (nset 'cycle 0))
              ;; if interval mode, kill the new flag whether
              ;; right or wrong
              (nset 'lnew 1))
            ;; question correct?
            (cond ((>= qscore ok-score)
                   ;; we always bump the lright counter in any mode
                   (nset 'lright (1+ (nref 'lright)))
                   (case operation-mode
                     (interval (nset
                                ;; change due date and cycle
                                'cycle (1+ (nref 'cycle))
                                'nextask (elip-find-nextask record)))
                     (leitner (nset
                               ;; increment Leitner box number
                               ;; (do nothing if card leaves the last box)
                               'lhole (1+ (nref 'lhole))))
                     (text (nset
                            ;; reduce the percentage factor
                            ;; (flag as done if we reach minimum)
                            'percent (let ((p (max pct-min
                                                   (- (nref 'percent)
                                                      elip-text-decrement))))
                                       (if (<= p pct-min)
                                           -1
                                         p))))))
                  (t
                   ;; we missed, bump miss count in all modes
                   (nset 'lmissed (1+ (nref 'lmissed)))
                   (case operation-mode
                     (text (nset
                            ;; increase percentage fill,
                            ;; and go back all the way on a flub
                            'percent (if flubp
                                         pct-max
                                       (min pct-max
                                            (+ (nref 'percent)
                                               elip-text-decrement)))))
                     (leitner (nset
                               ;; flub pushes back to box 1, while
                               ;; near miss pushes back one box only
                               'lhole (if flubp
                                          1
                                        (max 1 (1- (nref 'lhole))))))
                     (interval (push
                                ;; save for another sweep
                                record bad))))))
          (db-accept-record)
          (db-next-record 0)))
      ;; did we get them all correct?
      ;; (in non interval mode ‘bad’ is always nil)
      (when (setq lls bad)
        (elip-hey "Repeating %d missed items" (length lls)))))
  (database-set-modified-p dbc-database t)
  (db-save-database)
  (db-next-record 0))

(defun elip-learn-text ()
  "Learn to memorize passages in context."
  (interactive)
  (let ((morep t)
        (max-new (database-get-local 'max-new dbc-database))
        (pct-max (database-get-local 'text-percent-max dbc-database))
        new-found learn-item)
    (while morep
      (elip-check-db t)
      (setq new-found 0)
      (unless (catch 'incomplete
                (db-maprecords
                 (lambda (record)
                   (when (>= (db-record-field record 'percent)
                             0)
                     (throw 'incomplete t))))
                nil)
        (error "Text mode complete for this database, reset first"))
      (elip-maprecords-with-index-until
       (lambda (record index)
         ;; looking for used unfinished text cards first:
         ;; if >0 percentage, since -1 is done and 0 is unused:
         (when (> (db-record-field record 'percent) 0)
           ;; usable record
           (push index learn-item)
           (incf new-found)
           (>= new-found max-new))))
      ;; if we don't have max-new used unfinished text cards,
      ;; we go back and add enough text-new cards to make up the
      ;; difference, or as many as we have left at least
      (when (< new-found max-new)
        (elip-maprecords-with-index-until
         (lambda (record index)
           (when (zerop (db-record-field record 'percent))
             ;; start at max percent
             (db-record-set-field record 'percent pct-max)
             (push index learn-item)
             (incf new-found)
             (>= new-found max-new)))))
      (when (and (> new-found 0)
                 (> max-new new-found))
        (elip-hey "Learning last %d text items" new-found))
      (setq morep
            (if (zerop new-found)
                (progn
                  ;; we're done!
                  (message "No text items left to learn, reset database")
                  nil)
              (elip-hey "Begin text learning")
              (elip-learn-list 'text (nreverse learn-item))
              ;; in text mode we don't rerun errors, so prompt for another run
              (yes-or-no-p "Text round complete, do more cards? "))))))

(defun elip-learn-flashcards ()
  "Learn practice flashcard style without time intervals and saved scores."
  (interactive)
  (elip-check-db t)
  (let* ((new-found 0)
         (max-new (database-get-local 'max-new dbc-database))
         (nrecords (database-no-of-records dbc-database))
         (idx+recs (make-vector nrecords nil))
         style learn-item)

    (elip-maprecords-with-index
     (lambda (record index)
       (aset idx+recs (1- index) (cons index record))))

    (while (not style)
      ;; Bob Newell sez: only all items works for now
      (setq style (read-char-exclusive
                   (concat "Which items? ("
                           (propertize "o" 'face 'region)
                           "ld / "
                           (propertize "n" 'face 'region)
                           "ew / "
                           (propertize "a" 'face 'region)
                           "ll / "
                           (propertize "h" 'face 'region)
                           "ardest): "))
            style (when (memq style '(?o ?n ?a ?h))
                    style)))

    ;; 'h' is way different
    (if (not (= ?h style))
        (let (pair)
          ;; sort, then pick out what we need
          (elip-shuffle-vector idx+recs)
          (do ((i 0 (1+ i)))
              ((or (= i nrecords)
                   (= new-found max-new)))
            (setq pair (aref idx+recs i))
            (cl-flet*
                ((yes! ()
                       (push (car pair) learn-item)
                       (incf new-found))
                 (try (all-0-p)
                      (let* ((record (cdr pair))
                             (tuple (mapcar (lambda (f)
                                              (db-record-field record f))
                                            ;; nb: originally ‘elip-new’
                                            '(lnew
                                              lhole
                                              percent))))
                        (when (eq (equal '(0 0 0) tuple) all-0-p)
                          (yes!)))))
              (case style
                (?a (yes!))
                ;; this has been corrected - if tried in any mode it is old
                (?o (try nil))
                ;; corrected - it is only new if never tried in any mode
                (?n (try t))))))
      ;; else if h:
      (let (idx+mc times-missed)
        ;; first find all items with at least one miss
        (elip-maprecords-with-index
         (lambda (record index)
           (when (< 0 (setq times-missed
                            (- (db-record-field record 'trials)
                               (db-record-field record 'lright))))
             (push (cons index times-missed) idx+mc)
             (incf new-found))))
        ;; now sort by hardest.
        (setq idx+mc (sort idx+mc (lambda (a b)
                                    (> (cdr a) (cdr b)))))
        ;; still not done.  take the top group and put in learn-item
        ;; the max is either max-new or 20% database size.
        (dotimes (i (min new-found
                         max-new
                         (/ nrecords 5)))
          (push (car (pop idx+mc)) learn-item))))

    (if (zerop new-found)
        (message "No items meet your flashcard selection criteria")
      (elip-hey "%d item flashcard drill" new-found)
      (elip-learn-list 'flashcard (nreverse learn-item)))))

(defun elip-learn-new ()
  "Learn some new ELIP items"
  (interactive)
  (elip-check-db t)
  (let ((new-found 0)
        (max-new (database-get-local 'max-new dbc-database))
        learn-item)
    (elip-maprecords-with-index
     (lambda (record index)
       ;; Bob Newell sez:
       ;; We must distinguish a new non-Leitner item from a new Leitner item.
       ;; And, they both could be going on at once.  Attempts, time, trials, all
       ;; won't work because both modes track them.  So we will have to chew up
       ;; an aux variable for this.  And this will also make sorting messier.  A
       ;; new Leitner item is easy, it just has the Leitner box = 0.
       (when (zerop (db-record-field record 'lnew))
	 (unless (>= new-found max-new)
	   (push index learn-item))
         (incf new-found))))
    (if (zerop new-found)
        (message "No new items to learn")
      (if (>= new-found max-new)
	  (elip-hey "Learning %d of %d new items" max-new new-found)
	(elip-hey "%d new items to learn" new-found))
      (elip-learn-list 'interval (nreverse learn-item)))))

(defun elip-learn-leitner ()
  "Learn items Leitner flashcard style"
  (interactive)
  (let ((morep t)
        (max-new (database-get-local 'max-new dbc-database))
        (last-box (database-get-local 'leitner-last-box dbc-database))
        new-found learn-item)
    (while morep
      (elip-check-db t)
      (setq new-found 0)
      (unless (catch 'incomplete
                (db-maprecords
                 (lambda (record)
                   (when (<= (db-record-field record 'lhole)
                             last-box)
                     (throw 'incomplete t))))
                nil)
        (error "Leitner mode complete for this database, reset first"))
      (elip-maprecords-with-index-until
       (lambda (record index)
         ;; looking for used unfinished leitner cards first:
         ;; if not zero (new) and less than or eq max position (done):
         (when (and (<= (db-record-field record 'lhole)
                        last-box)
                    (not (zerop (db-record-field record 'lhole))))
           ;; usable record
           (push index learn-item)
           (incf new-found)
           (>= new-found max-new))))
      ;; if we don't have max-new used unfinished Leitner cards,
      ;; we go back and add enough leitner-new cards to make up the
      ;; difference, or as many as we have left at least
      (when (< new-found max-new)
        (elip-maprecords-with-index-until
         (lambda (record index)
           (when (zerop (db-record-field record 'lhole))
             (db-record-set-field record 'lhole 1)
             (push index learn-item)
             (incf new-found)
             (>= new-found max-new)))))
      (when (and
             (> new-found 0)
             (> max-new new-found))
        (elip-hey "Learning last %d Leitner items" new-found))
      (setq morep
            (if (zerop new-found)
                (progn
                  ;; we're done!
                  (message "No Leitner items left to learn, reset database")
                  nil)
              (elip-hey "Begin Leitner learning")
              (elip-learn-list 'leitner (nreverse learn-item))
              ;; in leitner mode we don't rerun errors, so prompt for another run
              (yes-or-no-p "Leitner round complete, do more cards? "))))))

(defun elip-learn-old ()
  "Learn some old ELIP items"
  (interactive)
  (elip-check-db t)
  (let ((new-found 0)
        (max-new (database-get-local 'max-new dbc-database))
        (curdate (elip-current-date))
        learn-item due-date)
    (elip-maprecords-with-index
     ;; fish out date convert compare to current date
     ;; definitely a midnight overlap problem can exist here in a rare case
     ;; --bug fix-- also must not be a delayed new item
     (lambda (record index)
       (setq due-date (db-record-field record 'nextask))
       (setq due-date (list (car (cdr due-date))
                            (cdr (cdr due-date))
                            (car due-date)))
       (setq due-date (calendar-absolute-from-gregorian due-date))
       ;; got a due item here but must not be new
       (when (and (<= due-date curdate)
                  (not (zerop (db-record-field record 'lnew))))
         (unless (>= new-found max-new)
	   (push index learn-item))
         (incf new-found))))
    (if (zerop new-found)
        (message "No review items to learn at this time")
      (if (>= new-found max-new)
	  (elip-hey (concat "Learning %d of %d review items; "
			    "rerun this command for more")
		    max-new new-found)
	(elip-hey "%d review items to learn" new-found))
      (elip-learn-list 'interval (nreverse learn-item)))))

(defun elip-make-database (filename topic sequentially)
  "Create database FILENAME for learning TOPIC, maybe SEQUENTIALLY.
FILENAME must not name an existing file (error if already exists).
TOPIC is a brief (one-line) string describing the topic of study.
SEQUENTIALLY, if non-nil, arranges for items to be studied in order
of the database entries.  The default is random order.  When
called interactively, use prefix arg to specify SEQUENTIALLY."
  (interactive "FCreate ELIP database: \nsTopic: \nP")
  (when (file-exists-p filename)
    (error "File already exists: %s" filename))
  (with-temp-file filename
    (insert ";;; " (file-name-nondirectory filename)
            " -*- mode:emacs-lisp; coding:utf-8; -*-\n\n"
            ":EDB (single elip-control-properties)\n\n"
            (format ":name\n%S\n\n" topic)
            ":display t\n"
            "*" topic "*\n"
            "
   Question: \\question
     Answer: \\answer

   Ask Next: \\nextask,date-mmddyy\n"
            ":EOTB\n\n"
            ":locals\n")
    (pp `[(text-percent-min 20)
          (text-percent-max 90)
          (leitner-last-box 5)
          (ok-score 4)
          (max-new 20)
          (elip-learn-random ,(not sequentially))]
        (current-buffer))
    (insert "\n"
            ":data (:coding t :seqr read-line :seqw write-line"
            " :EOTB \":EOTB\")")
    (print `["Sample Question" "Delete AFTER adding real questions"
             ,(let ((edb-t-timedate1:parse-date-default 'current-date))
                (edb-t-timedate1:parse-date-string nil))
             0 0 0 0 0 0 0 0 0 0]
           (current-buffer))
    (insert ":EOTB\n\n"
            ";;; " (file-name-nondirectory filename) " ends here\n"))
  (message "Wrote %s for studying %S in %s order"
           (file-name-nondirectory filename)
           topic
           (if sequentially "sequential" "random")))

(defun elip-report ()
  "Show summary level ELIP statistics"
  (interactive)
  (elip-check-db)
  (let ((rbuf (elip-child-buffer "report"))
        (curdate (elip-current-date))
        (items-studied 0)
        (total-attempts 0)
        (total-right 0)
        (sum-time 0)
        (sum-score 0)
        (overdue 0)
        (due-today 0)
        (due-tomorrow 0)
        (new-untried 0)
        (leitner-untried 0)
        (leitner-complete 0)
        (text-untried 0)
        (text-complete 0)
        (leitner-boxes (make-vector 25 0))
        (nrecords (database-no-of-records dbc-database))
        (last-box (database-get-local 'leitner-last-box dbc-database))
        percent-right)
    (db-maprecords
     (lambda (record)
       (cl-flet
           ((ref (f) (db-record-field record f)))
         ;; date stuff and new item count
         ;; there is some repeated code here but it just seems simpler
         ;; --new items undone--  first leitner stuff and text stuff
         (when (zerop (ref 'lhole))
           (incf leitner-untried))
         (when (zerop (ref 'percent))
           (incf text-untried))
         ;; then regular stuff --late, current, tomorrow -- but not new!
         (if (zerop (ref 'lnew))
             (incf new-untried)
           (let ((due-date (ref 'nextask)))
             (setq due-date (calendar-absolute-from-gregorian
                             (list (car (cdr due-date))
                                   (cdr (cdr due-date))
                                   (car due-date))))
             (if (< due-date curdate)
                 (incf overdue)
               (if (= due-date curdate)
                   (incf due-today)
                 (when (= due-date (1+ curdate))
                   (incf due-tomorrow))))))
         ;;
         ;; when doing total stats and the like, we
         ;; only bother with stuff already tried at least once
         ;;
         (when (or (not (zerop (ref 'lnew)))
                   (not (zerop (ref 'lhole)))
                   (not (zerop (ref 'percent))))
           ;; check for Leitner box
           (let ((leitner-value (ref 'lhole)))
             (when (> leitner-value 0)
               (incf (aref leitner-boxes leitner-value)))
             (when (> leitner-value last-box)
               (incf leitner-complete)))
           ;; check for text finished
           (when (< (ref 'percent) 0)
             (incf text-complete))
           (incf items-studied)
           (incf total-attempts (ref 'trials))
           (incf total-right    (ref 'lright))
           (incf sum-time       (ref 'ttime))
           (incf sum-score      (ref 'tscore))))))
    (if (zerop items-studied)
        (message "%s items in this database; none studied yet"
                 (database-no-of-records dbc-database))
      (setq percent-right (* 100 (/ (float total-right)
                                    (float total-attempts))))
      (set-buffer rbuf)
      (insert
       "\nELIP Summary Level Statistics\n\n"
       "(For individual item stats, use the command elip-item-stats)\n"
       "\n"
       "Items in database       : "
       (int-to-string nrecords)
       "\n"
       "Interval items untried  : "
       (int-to-string new-untried)
       "\n"
       "Text items untried      : "
       (int-to-string text-untried)
       "\n"
       "Text items complete     : "
       (int-to-string text-complete)
       "\n"
       "Leitner items untried   : "
       (int-to-string leitner-untried)
       "\n"
       "Leitner items complete : "
       (int-to-string leitner-complete)
       "\n"
       "Leitner box fill       : ")
      (dotimes (n (1+ last-box))
        (insert (format "%d  " (aref leitner-boxes (1+ n)))))
      (insert
       "\n"
       "Overdue items          : "
       (int-to-string overdue)
       "\n"
       "Items due today        : "
       (int-to-string due-today)
       "\n"
       "Items due tomorrow     : "
       (int-to-string due-tomorrow)
       "\n"
       "Number of items studied: "
       (int-to-string items-studied)
       "\n"
       "Total attempts         : "
       (int-to-string total-attempts)
       "\n")
      (when (setq percent-right (and (not (zerop total-attempts))
                                     (* 100 (/ (float total-right)
                                               (float total-attempts)))))
        (insert
         "Total correct answers  : "
         (int-to-string total-right)
         "\n"
         "Total incorrect answers: "
         (int-to-string (- total-attempts total-right))
         "\n"
         "Percent correct        : "
         (format "%7.2f" percent-right)
         "\n"
         "Avg. time per attempt  : "
         (format "%7.2f"
                 (/ (float sum-time) (float total-attempts)))
         " seconds\n"
         "Avg. score per attempt : "
         (format "%7.2f" (/ (float sum-score)
                            (float total-attempts)))
         "\n"
         "\nYour grade is "
         (cond ((>= percent-right 97) "A+")
               ((>= percent-right 94) "A")
               ((>= percent-right 90) "A-")
               ((>= percent-right 87) "B+")
               ((>= percent-right 84) "B")
               ((>= percent-right 80) "B-")
               ((>= percent-right 77) "C+")
               ((>= percent-right 74) "C")
               ((>= percent-right 70) "C-")
               ((>= percent-right 67) "D+")
               ((>= percent-right 64) "D")
               ((>= percent-right 60) "D-")
               (t                     "F"))
         ".\n"))
      (goto-char (point-min))
      (let (pop-up-windows)
        (pop-to-buffer rbuf)))))

(defun elip-check-files (i x)
  (when i (unless (file-readable-p i)
            (error "Can't access import file: %s" i)))
  (when x (when (or (not (file-writable-p x))
                    (and (not (file-exists-p x))
                         (not (yes-or-no-p (format "Overwrite %s? " x)))))
            (error "Can't, or won't, access or overwrite export file"))))

(defun elip-export (export-file)
  "Export questions and answers only to a flat file"
  (interactive "FExport to file: ")
  (elip-check-files nil export-file)
  (elip-check-db)
  (let ((db-filename (database-file dbc-database))
        ebuf)
    (save-excursion
      (setq ebuf (find-file export-file))
      (erase-buffer)
      (insert
       "Export of ELIP database "
       db-filename
       " on "
       (calendar-date-string (calendar-current-date))
       "\n"))
    (db-maprecords
     (lambda (record)
       (let ((q (db-record-field record 'question))
             (a (db-record-field record 'answer)))
         (with-current-buffer ebuf
           (goto-char (point-max))
           (insert "\n"
                   "Q. " q
                   "\nA. " a)))))
    (pop-to-buffer ebuf)
    ;; Now write the file out
    (write-file export-file)
    (goto-char (point-min))
    (message "Export complete")))

(defun elip-import (import-file)
  "Import new learning items from IMPORT-FILE.
Append items to the database and leave the current record at the
first imported item.  If IMPORT-FILE contains no valid Q/A pairs,
do nothing.  Lastly, display a count of the imported items."
  (interactive "fImport from file: ")
  (elip-check-db t)
  (let ((pbuf (current-buffer))
        (donep nil)
        (count 0)
        mark q-text a-text)
    (elip-check-files import-file nil)
    (db-last-record)
    (with-temp-buffer
      (insert-file-contents import-file)
      (while (not donep)
        ;; find a question marker
        (if (not (search-forward-regexp "^Q\\." nil t))
            (setq donep t)
          (setq mark (point))
          ;; a question is bounded by the answer marker
          (if (not (search-forward-regexp "^A\\." nil t))
              (setq donep t)
            ;; found end of question, save it
            (setq q-text (buffer-substring mark (match-beginning 0)))
            ;; search end of answer or EOF
            (setq mark (point))
            (when (search-forward-regexp "^Q\\." nil 1)
              (goto-char (match-beginning 0)))
            (setq a-text (buffer-substring mark (point)))
            ;; now create the new record
            (with-current-buffer pbuf
              (db-add-record t)
              (incf count)
              (db-first-field)
              (insert (db-string-trim-whitespace q-text))
              (db-next-field 1)
              (insert (db-string-trim-whitespace a-text))
              (db-next-field 1)
              (db-first-field))))))
    (if (zerop count)
        (message "Nothing imported")
      (db-previous-record (1- count))
      (db-view-mode)
      (message "Import complete: %d records" count))))

(defun elip-version ()
  "Show ELIP version and date"
  (interactive)
  (message "ELIP %s" elip-version))

(defun elip-item-stats ()
  "Show detailed report for every item in database"
  (interactive)
  (elip-check-db)
  (let ((last-box (database-get-local 'leitner-last-box dbc-database))
        (ibuf (elip-child-buffer "items"))
        (tbuf (elip-child-buffer "table")))
    ;; Put headers in table buffer
    (with-current-buffer tbuf
      (save-excursion
        (insert
         "   1         2       3    4    5      6     7      8     9      10 "))
      (while (re-search-forward "[0-9]+" nil 1)
        (make-button (match-beginning 0) (match-end 0)
                     'action (lambda (button)
                               (elip-table-sort
                                (string-to-number (button-label button))))))
      (insert
       "  11  12\n"
       " Item       Due     Tot  Tot  Tot    Pct   Tot    Avg   Tot     Avg  Ltnr Txt\n"
       "  No       Date     Att  Rgt  Wrg    Rgt   Tim    Tim   Scr     Scr  Box  Pct\n\n"))
    (let (due-date
          total-attempts due-nums total-right
          times-missed percent-right sum-time average-time
          sum-score average-score leitner-position text-percentage)
      (elip-maprecords-with-index
       (lambda (record index)
         (with-current-buffer ibuf
           (insert
            "====== Stats for item "
            (int-to-string index)
            " =====\n\n"))
         (setq due-date (db-record-field record 'nextask))
         (setq total-attempts (db-record-field record 'trials))
         ;; dual check, it isn't new for our purposes unless it has not been
         ;; tried either with Leitner or with LIP methods
         (if (and
              (zerop (db-record-field record 'lnew))
              (zerop (db-record-field record 'lhole))
              (zerop (db-record-field record 'percent)))
             ;; don't futz with new item
             (with-current-buffer ibuf
               (insert "*** UNTRIED NEW ITEM ***\n"))
           ;; go for it
           (setq due-date (list
                           (car (cdr due-date))
                           (cdr (cdr due-date))
                           (car due-date)))
           (setq due-nums (format "%4s-%2s-%2s"
                                  (substring (prin1-to-string
                                              (cdr (cdr due-date))) 1 5)
                                  (car due-date)
                                  (car (cdr due-date))))
           (setq due-nums (subst-char-in-string ?\s ?0 due-nums))
           (setq due-date (calendar-date-string due-date))
           (setq total-right (db-record-field record 'lright))
           (setq times-missed (- total-attempts total-right))
           (setq percent-right (* 100 (/ (float total-right)
                                         (float total-attempts))))
           (setq sum-time (db-record-field record 'ttime))
           (setq average-time (/ (float sum-time)
                                 (float total-attempts)))
           (setq sum-score (db-record-field record 'tscore))
           (setq average-score (/ (float sum-score)
                                  (float total-attempts)))
           (setq leitner-position (db-record-field record 'lhole))
           (setq text-percentage (db-record-field record 'percent))
           (with-current-buffer ibuf
             (insert
              "Next repetition date: " due-date
              "\nTimes attempted     : "
              (int-to-string total-attempts)
              "\nTimes correct       : "
              (int-to-string total-right)
              "\nTimes missed        : "
              (int-to-string times-missed)
              "\nPercent correct     : "
              (format "%-5.2f" percent-right)
              "\nTotal time          : "
              (format "%-7d" sum-time) " seconds"
              "\nAverage time        : "
              (format "%-7.2f" average-time) " seconds"
              "\nTotal score         : "
              (int-to-string sum-score)
              "\nAverage score       : "
              (format "%-7.2f" average-score)
              "\nLeitner box         : ")
             (if (> leitner-position last-box)
                 (insert "*Done*")
               (insert (format "%-2d" leitner-position))
               ;; a kludge to make a 'done' marker in the table below
               (setq leitner-position 99))
             (insert
              "\nText percentage     : "
              (if (< text-percentage 0)
                  "*Done*"
                (format "%-2d" text-percentage))))
           ;; now build the table buffer, still old items only
           (with-current-buffer tbuf
             (insert
              (format
               "%4d  %12s %4d %4d %4d  %6.2f %4d %7.2f %4d %7.2f    %2d %2d\n"
               index
               due-nums
               total-attempts
               total-right
               times-missed
               percent-right
               sum-time
               average-time
               sum-score
               average-score
               leitner-position
               text-percentage))))
         ;; show q and a regardless if old or new just in item buffer
         (let ((q (db-record-field record 'question))
               (a (db-record-field record 'answer)))
           (with-current-buffer ibuf
             (insert
              "\n\nQuestion:\n" q
              "\nAnswer:\n" a
              "\n\n"))))))
    ;; finish in item buffer
    (let (pop-up-windows)
      (pop-to-buffer tbuf)
      (goto-char (point-min)))
    (pop-to-buffer ibuf)
    (goto-char (point-min))))

(defun elip-reset ()
  "Restart learning on this database fully or partly."
  (interactive)
  (elip-check-db t)
  (let ((mode t))
    (while mode
      (case (setq mode (read-char-exclusive
                        (concat "Reset ("
                                (propertize "l" 'face 'region)
                                "eitner / "
                                (propertize "t" 'face 'region)
                                "ext / "
                                (propertize "i" 'face 'region)
                                "nterval / "
                                (propertize "a" 'face 'region)
                                "ll / "
                                (propertize "q" 'face 'region)
                                "uit): ")))
        (?l (db-maprecords
             (lambda (record)
               (db-record-set-field record 'lhole 0))))
        (?t (db-maprecords
             (lambda (record)
               (db-record-set-field record 'percent 0))))
        (?i (db-maprecords
             (lambda (record)
               ;; nb: originally ‘elip-new’
               (db-record-set-field record 'lnew 0)
               (db-record-set-field record 'cycle 0)
               (db-record-set-field record 'nextask (elip-current-date t)))))
        (?a (when (yes-or-no-p "REALLY do this? ")
              (db-maprecords
               (lambda (record)
                 (let ((plist (elip-new-record-plist)))
                   (while plist
                     (let ((k (pop plist))
                           (v (pop plist)))
                       (db-record-set-field record k v dbc-database))))))))
        (?q (setq mode nil)))))
  ;; force redisplay
  (db-previous-record 0))

(defun elip-help ()
  "Display help on ELIP in a new buffer.
Include buttons to start a new session or to resume an ongoing session."
  (interactive)
  (let (pop-up-windows)
    (pop-to-buffer "*ELIP Help*"))
  (let ((inhibit-read-only t)
        ls)
    (erase-buffer)
    (insert "ELIP (Emacs Learning Interval Program) " elip-version "\n")
    (insert "\n[")
    (insert-text-button
     "Start a session"
     'action (lambda (button)
               (call-interactively 'elip-session)))
    (insert "]")
    (dolist (buf (buffer-list))
      (with-current-buffer buf
        (when (gethash dbc-database elip-known-databases)
          (push buf ls))))
    (if (not ls)
        (insert "\n")
      (insert " or resume an ongoing session:\n")
      (dolist (buf ls)
        (insert "  [")
        (let ((name (database-print-name (with-current-buffer buf
                                           dbc-database))))
          (insert-text-button
           name
           :elip-buffer buf
           'action (lambda (button)
                     (let ((buf (button-get button :elip-buffer)))
                       (if (buffer-live-p buf)
                           (let (pop-up-windows)
                             (pop-to-buffer buf))
                         (elip-help)
                         (message "(Oops, %S no longer ongoing.)"
                                  (button-label button))))))
          (insert "]\n"))))
    (insert "
Commands:
  elip-help               This help screen
  elip-learn-new          Learn new items in interval mode
  elip-learn-old          Learn 'due' items in interval mode
  elip-learn-leitner      Learn Leitner flashcard style
  elip-learn-text         Learn text memorization style
  elip-learn-flashcards   Do a flashcard-style drill
  elip-report             Report on summary learning stats
  elip-workload           Report on items due by date
  elip-version            Show ELIP version number
  elip-import             Import questions/answers from flat file
  elip-export             Export questions/answers to flat file
  elip-reverse            Reverse questions and answers to/from flat files
  elip-item-stats         Show statistics with each item
  elip-table-sort         Sort an item stats table
  elip-reset              Reset the database and restart learning
  elip-scan-text          Prepare questions from marked text file

Scoring scale for questions:
  0  Clueless                      3  Near miss or partly wrong but familiar
  1  Fully wrong and not familiar  4  Correct with hesitation
  2  Fully wrong but familiar      5  Correct and quick"))
  (goto-char (point-min))
  (help-mode)
  (setq view-exit-action 'kill-buffer)
  (forward-button 1))

(defun elip-find-new-text-of-interest ()
  "Return next text between {} markers."
  ;; called from non-ELIP buffer by definition
  (when (search-forward "{" nil t)
    (let ((start (point)))
      (when (search-forward "}" nil t)
        (buffer-substring start (1- (point)))))))

(defun elip-scan-text ()
  "Scan text and make up question and answer buffer"
  (interactive)
  ;; called from non-ELIP buffer by definition
  (let ((ibuf (buffer-name))
        (obuf (get-buffer-create (read-from-minibuffer
                                  "Output buffer: ")))
        new-q clean-new-q new-a
        match-count match-start saved-answers
        L R)
    ;; scan and write

    ;; see if we have a group to work on
    (while (setq new-q (elip-find-new-text-of-interest))
      ;; generate a question free of brackets while saving answers
      (setq clean-new-q "")
      (setq match-count 0)
      (setq match-start 0)
      (setq saved-answers (make-vector 100 ""))
      ;; can we find a left bracket and repeat
      (while (setq L (string-match "[[]" new-q match-start))
        ;; can we find a right bracket
        (when (setq R (string-match "]" new-q L))
          (setq clean-new-q
                (concat clean-new-q (substring new-q match-start L)))
          ;; save up to the left bracket
          (setq new-a (substring new-q (1+ L) R))
          ;; save the answer onto the question string
          (setq clean-new-q
                (concat clean-new-q new-a))
          ;; stow the answer in a list
          (aset saved-answers match-count new-a)
          ;; bump pointer and counters
          (incf match-count)
          (setq match-start (1+ R))))
      ;; now put in the last piece of the question which follows the last brackets
      (setq clean-new-q
            (concat clean-new-q
                    (substring new-q match-start)))
      ;; enter this group into the q/a buffer, fixing the question as we go
      (with-current-buffer obuf
        (goto-char (point-max))
        ;; we do the regular q/a thing if we found at least one [] group
        (cond ((> match-count 0)
               (dotimes (i match-count)
                 (insert "\nQ. ")
                 ;; now fix the question to do the ellipsis thing
                 (let ((nice-answer (aref saved-answers i)))
                   (insert
                    (substring clean-new-q 0
                               (string-match nice-answer clean-new-q))
                    " [...] "
                    (substring clean-new-q (match-end 0))
                    "\nA. "
                    nice-answer
                    "\n"))))
              (t
               ;; if there are no [] groups we treat it as a text group,
               ;; and use the whole passage as a text Q. with a blank A.
               (insert
                "\nQ. "
                new-q
                "\nA. \n")))))
    (pop-to-buffer obuf)
    (goto-char (point-min))
    (message "Import file constructed, edit as needed, save to disk, and import")))

(defun elip-sort ()
  "Sub for db-sort"
  (interactive)
  (elip-check-db)
  (let ((seq (make-hash-table :test 'eq)))
    (elip-maprecords-with-index
     (lambda (record index)
       (puthash record index seq)))
    (database-sort
     dbc-database
     (lambda (a b)
       (let ((anew (zerop (aref a 7)))
             (bnew (zerop (aref b 7))))
         ;; a new, b old return t
         (if (and anew (not bnew))
             t
           ;; a old, b new return nil
           (if (and (not anew) bnew)
               nil
             (let ((adate (edb-t-timedate1:date->absolute-days (aref a 2)))
                   (bdate (edb-t-timedate1:date->absolute-days (aref b 2)))
                   (aseq (gethash a seq))
                   (bseq (gethash b seq)))
               ;; both old - sort by date and sequence
               (if (and (not anew) (not bnew))
                   (if (= adate bdate)
                       (< aseq bseq)
                     (< adate bdate))
                 ;; fall through to both new, sort by sequence only
                 (< aseq bseq)))))))))
  (db-jump-to-record 1))

(defun elip-shuffle ()
  "Shuffle all the items"
  (interactive)
  (elip-check-db)
  ;; Really shuffle the new items.  Shuffle everything and then
  ;; sort.  This brings shuffled new items to the top and puts
  ;; the old items back in proper order.
  (let ((seq (make-hash-table :test 'eq)))
    (db-maprecords
     (lambda (record)
       (puthash record (random) seq)))
    (database-sort dbc-database (lambda (a b)
                                  (< (gethash a seq)
                                     (gethash b seq)))))
  (elip-sort))

(defun elip-reverse (import-file export-file)
  "Reverse learning items from a flat file to a flat file"
  (interactive "fImport normal items from: \nFExport reversed items to: ")
  (let ((count 0)
        (donep nil)
        ibuf xbuf mark q-text a-text)
    (elip-check-files import-file export-file)
    (find-file export-file)
    (setq xbuf (buffer-name))
    (erase-buffer)
    (insert "ELIP reversed questions database ")
    (find-file import-file)
    (setq ibuf (buffer-name))
    (goto-char (point-min))
    (while (not donep)
      (if (not (search-forward-regexp "^Q\\." nil t))
          (setq donep t)
        (setq mark (point))
        ;; a question is bounded by the answer marker
        (if (not (search-forward-regexp "^A\\." nil t))
            (setq donep t)
          ;; found end of question, save it
          (backward-char 3)
          (setq q-text (buffer-substring mark (point)))
          ;; search end of answer or EOF
          (forward-char 3)
          (setq mark (point))
          (when (search-forward-regexp "^Q\\." nil 1)
            (backward-char 3))
          (setq a-text (buffer-substring mark (point)))
          ;; now create the new record
          (with-current-buffer xbuf
            (insert
             "\nQ. "
             a-text
             "\nA. "
             q-text
             "\n"))
          (incf count))))
    (if (zerop count)
        (message "Nothing exported")
      (let (pop-up-windows)
        (pop-to-buffer xbuf))
      (goto-char (point-min))
      (write-file export-file)
      (message "Generation of reversed items complete: %d items" count))))

(defun elip-workload ()
  "Show report of number of items due by date"
  (interactive)
  (elip-check-db)
  (elip-sort)
  (let ((wbuf (elip-child-buffer "workload"))
        (seen (make-hash-table :test 'equal))
        ordered)
    (db-maprecords
     (lambda (record)
       ;; old records only!  dates on new items don't matter here
       (unless (zerop (db-record-field record 'trials))
         (incf (gethash (db-record-field record 'nextask) seen 0)))))
    (with-current-buffer wbuf
      (insert (format "\n%-30s  Item Count\n\n" "Item Due Date"))
      (maphash (lambda (date count)
                 (push (cons (edb-t-timedate1:date->absolute-days date)
                             (cons date count))
                       ordered))
               seen)
      (dolist (pair (mapcar 'cdr (sort ordered (lambda (a b)
                                                 (< (car a) (car b))))))
        (insert
         (format "%-30s  %2d\n"
                 (edb-t-timedate1:format-date
                  "%weekday, %month %d, %year" (car pair))
                 (cdr pair)))))
    (let (pop-up-windows)
      (pop-to-buffer wbuf))
    (goto-char (point-min))
    (when (null ordered)
      (message "(No items studied yet in this database)"))))

(defun elip-table-sort (&optional field)
  "Sort an item stats table by FIELD, an integer.
Except for item no and due date, sort in descending order.
FIELD can be one of:
   1 -- item no              6 -- pct right
   2 -- due date             7 -- total time
   3 -- total attempts       8 -- avg time
   4 -- total right          9 -- total score
   5 -- total wrong         10 -- avg score
If unspecified, it defaults to 1 (item number)."
  (interactive "nSort field (1-10): ")
  (unless (and (<= 1 field) (>= 10 field))
    (error "Invalid sort field"))
  (save-excursion (elip-check-db))
  (unless (string-match " table$" (buffer-name))
    (error "Doesn't appear to be an ELIP item stats table buffer"))
  (save-excursion
    (goto-char (point-min))
    (search-forward "\n\n")
    (funcall (if (= 2 field)
                 'sort-fields
               'sort-numeric-fields)
             field (point) (point-max))
    (unless (= 1 field)
      (reverse-region (point) (point-max)))))

(defun elip-set-max-items-at-once ()
  "Set the maximum number of items to study at once."
  (interactive)
  (elip-check-db t)
  (let (n)
    (while (progn
             (setq n (read-number "Max number of items to learn at once "
                                  (database-get-local 'max-new dbc-database)))
             (or (> 2 n)
                 (< 50 n)))
      (message "Out of range (2-50)")
      (sit-for 2))
    (database-set-local 'max-new dbc-database n)
    (db-write-database-file (database-file dbc-database) t)))

(defun elip-dump-elisp ()
  "Display Emacs Lisp code from an ELIP database in another buffer."
  (interactive)
  (elip-check-db)
  (let ((dbuf (elip-child-buffer "dump"))
        (db-filename (database-file dbc-database))
        elisp-p q-text a-text)
    (cl-flet
        ((dbuf-insert (&rest args) (with-current-buffer dbuf
                                     (apply 'insert args))))
      (dbuf-insert
       "Dump of elisp code in ELIP database "
       db-filename
       "\non "
       (calendar-date-string (calendar-current-date))
       "\n")
      (elip-maprecords-with-index
       (lambda (record index)
         (when (string-match "^#####" (setq q-text (db-record-field
                                                    record 'question)))
           (setq elisp-p t)
           (dbuf-insert
            "\n **** Elisp code found in record "
            (int-to-string index)
            " question text ****\n\n"
            q-text
            "\n"))
         (when (string-match "^#####" (setq a-text (db-record-field
                                                    record 'answer)))
           (setq elisp-p t)
           (dbuf-insert
            "\n **** Elisp code found in record "
            (int-to-string index)
            " answer text ****\n\n"
            a-text
            "\n")))))
    (if elisp-p
        (let (pop-up-windows)
          (pop-to-buffer dbuf))
      (kill-buffer dbuf)
      (message "(No Emacs Lisp code in this database)"))))

(provide 'elip)

;;; elip.el ends here
